const { createContext } = require('react');

const authContext = createContext({
  isAuthenticated: false,
  login: () => {},
  logout: () => {},
});

export default authContext;

import React from 'react';
import Layout from '../../../components/shared/Layout';
import QuestionSidebar from '../../../components/Qusetion/QuestionSidebar';
import QuestionItem from '../../../components/Awnsers/QuestionItem';
import AwsnersList from '../../../components/Awnsers/AwsnersList';
import RelatedQuestions from '../../../components/Awnsers/RelatedQuestions';
import Head from 'next/head';

function awnsers() {
  return (
    <Layout>
      <Head>
        <title>صفحه پاسخ حقوقی</title>
      </Head>
      <div class='QA-page text-right'>
        <div class='container'>
          <div class='row'>
            <div class='col-md-9'>
              <QuestionItem />
              <div class='awnsers-wrap'>
                <span>5 پاسخ برای این پرسش ارسال شده است</span>
                <AwsnersList />
                <div class='auth-call-action'>
                  <div>
                    <span>
                      چنانچه وکیل یا متخصص حقوقی هستید میتوانید با ورود به حساب
                      کاربریتان، پساخ خود به این پرسش را ثبت کنید.
                    </span>
                  </div>
                  <div class='mt-3 mb-2 text-center'>
                    <a href='' class='QA-form-btn'>
                      <span>ورود یا ثبت نام</span>
                    </a>
                  </div>
                </div>
              </div>
              <RelatedQuestions />
            </div>
            <div class='col-md-3'>
              <QuestionSidebar />
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
}

export default awnsers;

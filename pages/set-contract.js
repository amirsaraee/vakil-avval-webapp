import React from 'react';
import { Pagination } from 'antd';
import Layout from '../components/shared/Layout';
import ContractCard from '../components/ContractPage/ContractCard';
import SearchFilter from '../components/ContractPage/SearchFilter';
import ContractItem from '../components/ContractPage/ContractItem';
import Head from 'next/head';

function setContract() {
  return (
    <Layout>
      <Head>
        <title>صفحه تنظیم قرار داد</title>
      </Head>
      <div class='page-section text-right'>
        <div class='container'>
          <div class='card-section'>
            <span class='item-title'>
              تنظیم / بازبینی قرارداد طبق سفارش و شرایط درخواستی شما
            </span>
            <div class='card-box'>
              <ContractCard
                type='تنظیم'
                title='قرارداد داخلی'
                text=' تنظیم متن قراردادهای داخلی با زبان فارسی و انگلیسی'
                price='از 100 تا 200 هزار تومان'
              />
              <ContractCard
                type='تنظیم'
                title='قرارداد بین المللی'
                text='  تنظیم متن قراردادهای بین المللی به زبان فارسی و یا انگلیسی'
                price='از 100 تا 200 هزار تومان'
              />
              <ContractCard
                type='بازبینی'
                title='قرارداد داخلی'
                text='  بازبینی و اصلاح متن قراردادهای داخلی به زبان فارسی و یا انگلیسی'
                price='از 100 تا 200 هزار تومان'
              />
              <ContractCard
                type='تنظیم'
                title='بازبینی'
                text=' بازبینی و اصلاح متن قراردادهای بین المللی به زبان فارسی و یا انگلیسی'
                price='از 100 تا 200 هزار تومان'
              />
              <ContractCard
                type=' تنظیم / بازبینی '
                title='قرارداد خاص'
                text='  تنظیم و بازبینی متن قراردادهای داخلی و بین المللی حاوی شرایط خاص مثل زبان های خاص و غیره. '
                price='از 100 تا 200 هزار تومان'
              />
            </div>
          </div>
          <div class='select-lawyer'>
            <span class='item-title'>
              مشاهده و دانلود نمونه قراردادهای داخلی و بین المللی
            </span>

            <SearchFilter />
            <div class='paper-box'>
              <div class='row'>
                <div class='col-md-4'>
                  <ContractItem />
                </div>
                <div class='col-md-4'>
                  <ContractItem />
                </div>
                <div class='col-md-4'>
                  <ContractItem />
                </div>
                <div class='col-md-4'>
                  <ContractItem />
                </div>
                <div class='col-md-4'>
                  <ContractItem />
                </div>
                <div class='col-md-4'>
                  <ContractItem />
                </div>
                <div class='col-md-4'>
                  <ContractItem />
                </div>
                <div class='col-md-4'>
                  <ContractItem />
                </div>
                <div class='col-md-4'>
                  <ContractItem />
                </div>
              </div>
              <div class='Search-result-pagination'>
                <Pagination defaultCurrent={1} total={50} />
              </div>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
}

export default setContract;

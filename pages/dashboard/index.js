import React from 'react';
import Head from 'next/head';
import Layout from '../../components/shared/Layout';
import Sidebar from '../../components/Dashboard/Sidebar';
import UserInfo from '../../components/Dashboard/UserInfo';

function index() {
  return (
    <Layout>
      <Head>
        <title>داشبورد</title>
      </Head>
      <div className='py-5 text-right'>
        <div className='container'>
          <div className='row'>
            <div className='col-md-3'>
              <Sidebar defaultKey={1} />
            </div>
            <div className='col-md-9'>
              <UserInfo />
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
}

export default index;

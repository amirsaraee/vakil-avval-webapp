import React from 'react';
import Head from 'next/head';
import Layout from '../../components/shared/Layout';
import Sidebar from '../../components/Dashboard/Sidebar';
import LawServices from '../../components/Dashboard/LawServices';

function services() {
  return (
    <Layout>
      <Head>
        <title>خدمات حقوقی شما</title>
      </Head>
      <div className='py-5 text-right'>
        <div className='container'>
          <div className='row'>
            <div className='col-md-3'>
              <Sidebar defaultKey={2} />
            </div>
            <div className='col-md-9'>
              <LawServices />{' '}
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
}

export default services;

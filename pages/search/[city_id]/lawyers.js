import React from 'react';
import SearchFilter from '../../../components/Search/SearchFilter';
import LawyersList from '../../../components/Search/LawyersList';
import Layout from '../../../components/shared/Layout';

function lawyers() {
  return (
    <Layout>
      <div class='serach-result-page text-right'>
        <div class='container'>
          <div class='search-result-page-haed'>
            <div class='blog-sidebar-head'>
              <h4>
                <span>همه وکلا و متخصصین حقوقی</span>
              </h4>
            </div>
            <SearchFilter />
          </div>

          <LawyersList />
        </div>
      </div>
    </Layout>
  );
}

export default lawyers;

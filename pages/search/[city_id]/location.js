import React from 'react';
import LawyerCardGrid from '../../../components/Homepage/LawyerCardGrid';
import LocationsList from '../../../components/Search/LocationsList';
import Layout from '../../../components/shared/Layout';

function location() {
  return (
    <Layout>
      <div className='search-page-wrapper text-right'>
        <div className='container'>
          <div>
            <div className='box-title'>
              <span>پرامتیازترین وکلا و مشاورین حقوقی</span>
            </div>
            <LawyerCardGrid count={4} />
          </div>
          <div className='mt-3'>
            <div className='search-page-head'>
              <span>همه نشانی های دفاتر حقوقی تهران</span>
            </div>
            <LocationsList />
          </div>
        </div>
      </div>
    </Layout>
  );
}

export default location;

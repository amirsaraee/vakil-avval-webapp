import React from 'react';
import LawyerCardGrid from '../../../components/Homepage/LawyerCardGrid';
import Layout from '../../../components/shared/Layout';
import ExpertiseList from '../../../components/Search/ExpertiseList';

function expertise() {
  return (
    <Layout>
      <div className='search-page-wrapper text-right'>
        <div className='container '>
          <div>
            <div class='box-title'>
              <span>پرامتیازترین وکلا و مشاورین حقوقی</span>
            </div>
            <LawyerCardGrid count={4} />
          </div>
          <div class='mt-3'>
            <div class='search-page-head'>
              <span>همه تخصص های حقوقی تهران</span>
            </div>
            <ExpertiseList />
          </div>
        </div>
      </div>
    </Layout>
  );
}

export default expertise;

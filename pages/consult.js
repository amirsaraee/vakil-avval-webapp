import React from 'react';
import Head from 'next/head';
import Layout from '../components/shared/Layout';
import SearchFilter from '../components/filter/SearchFilter';
import LawyerList from '../components/filter/LawyerList';
import PageLawyersGrid from '../components/shared/PageLawyersGrid';

function consult() {
  return (
    <Layout>
      <Head>
        <title>صفحه مشاوره حقوقی</title>
      </Head>
      <div class='page-section text-right'>
        <div class='container'>
          <div className='lawyer-slider'>
            <div class='box-title'>
              <span>پرامتیازترین وکلا و مشاورین حقوقی تهران</span>
            </div>
            <PageLawyersGrid count={4} />
          </div>
          <div class='select-lawyer'>
            <span class='item-title'>انتخاب مشاور حقوقی جهت مشاوره</span>
            <SearchFilter />
            <LawyerList />
          </div>
        </div>
      </div>
    </Layout>
  );
}

export default consult;

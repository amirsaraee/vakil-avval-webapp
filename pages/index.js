import Link from 'next/link';
import Head from 'next/head';
import NavHomePage from '../components/Homepage/NavHomePage';
import HeaderHomePgae from '../components/Homepage/HeaderHomePgae';
import ServiceCard from '../components/Homepage/ServiceCard';
import LawyerCardGrid from '../components/Homepage/LawyerCardGrid';
import TopLawyerItem from '../components/Homepage/TopLawyerItem';
import SectionHead from '../components/Homepage/SectionHead';
import QuestionListItem from '../components/Homepage/QuestionListItem';
import TopQuestionItem from '../components/Homepage/TopQuestionItem';
import BlogGrid from '../components/Homepage/BlogGrid';
import IntroduceBox from '../components/Homepage/IntroduceBox';
import IconItem from '../components/Homepage/IconItem';
import Footer from '../components/shared/Footer';
import BannerTop from '../components/shared/BannerTop';

export default function Home() {
  return (
    <>
      <Head>
        <title>وکیل اول</title>
      </Head>
      <BannerTop />
      <NavHomePage />
      <HeaderHomePgae />
      <div className='wrapper'>
        {/* Services Section */}
        <section className='main-section'>
          <div className='services'>
            <div className='container'>
              <ServiceCard />
            </div>
          </div>
        </section>

        {/* POPULAR LAWYERS */}
        <section className='main-section'>
          <div className='container'>
            <div className='row'>
              <div className='col-md-8 text-right'>
                <SectionHead
                  title='پربازدیدترین وکلا و مشاورین حقوقی محدوده شما'
                  url='search/[city_id]/lawyers'
                  as={`/search/124/lawyers`}
                />

                <LawyerCardGrid count={3} />
              </div>

              <div className='col-md-4'>
                <SectionHead
                  title='برترین متخصصین حقوقی'
                  url='search/[city_id]/lawyers'
                  as={`/search/124/lawyers`}
                />

                <div className=''>
                  <div className='top-lawyers'>
                    <TopLawyerItem
                      name='موسسه حقوقی عدالت'
                      expert=''
                      image='lawyer-3.jpg'
                      comments='20'
                      views='800'
                    />
                    <TopLawyerItem
                      name='حسن حسنی'
                      expert='وکیل پایه یک دادگستری'
                      image='lawyer-1.jpg'
                      comments='12'
                      views='500'
                    />
                    <TopLawyerItem
                      name='محمد محمدی'
                      expert='کارشناس ارشد حقوق جزا'
                      image='lawyer-2.jpg'
                      comments='8'
                      views='240'
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        {/* QUESTION BOX */}
        <section className='main-section'>
          <div className='container'>
            <div className='row'>
              <div className='col-md-8'>
                <div className='content-box'>
                  <SectionHead
                    title='جدیدترین پرسش ها از وکیل اول'
                    url='question'
                  />
                  <div className='question-box'>
                    <QuestionListItem
                      name='حسن حسنی'
                      expert=' وکیل پایه یک دادگستری'
                      image='lawyer-1.jpg'
                      time=' 10 دقیقه پیش'
                      comments='4'
                      views='100'
                      question=' چگونه میتوانیم اطلاعات حقوقی را دریافت کنیم؟'
                      awnser=' لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.'
                    />
                    <QuestionListItem
                      name='محمد محمدی'
                      expert=' کارشناس ارشد حقوق جزا '
                      image='lawyer-2.jpg'
                      time=' 30 دقیقه پیش'
                      comments='2'
                      views='70'
                      question=' چگونه میتوانیم اطلاعات حقوقی را دریافت کنیم؟'
                      awnser=' لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.'
                    />
                  </div>
                </div>
              </div>
              <div className='col-md-4 text-right top-qusestions'>
                <SectionHead
                  title='پربازدیدترین پرسش های حقوقی'
                  url='question'
                />
                <div className='top-questions-box'>
                  <TopQuestionItem />
                  <TopQuestionItem />
                  <TopQuestionItem />
                </div>
              </div>
            </div>
          </div>
        </section>

        {/* BLOG SECTION */}
        <section className='main-section' id='blog-section'>
          <div className='container'>
            <div className='section-head'>
              <div className='section-title'>
                <span>تازه ترین مطالب وکیل اول</span>
              </div>
              <Link href='/blog'>
                <a>
                  <span>مشاهده همه</span>
                  <i className='fas fa-angle-left mr-1'></i>
                </a>
              </Link>
            </div>
            <BlogGrid />
          </div>
        </section>

        {/* INTRODUCE BOX */}
        <section className='main-section'>
          <div className='container'>
            <IntroduceBox />
          </div>
        </section>

        {/* ICON BOX */}
        <section className='main-section'>
          <div className='logo-box'>
            <div className='container'>
              <div className='row'>
                <div className='col-md-4'>
                  <IconItem
                    text='دریافت مشورت حقوقی از وکلای دارای پروانه وکالت معتبر'
                    icon='Untitled-5.png'
                  />
                </div>
                <div className='col-md-4'>
                  <IconItem
                    text=' تضمین بازگشت حق المشاوره در صورت عدم رضایت از مشاوره حقوقی '
                    icon='money-back.png'
                  />
                </div>
                <div className='col-md-4'>
                  <IconItem
                    text=' امنیت اطلاعات کاربران '
                    icon='safe-user.png'
                  />
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>

      {/* SUPPORT BOX */}
      <div className='support-box'>
        <div>
          <span>جهت پشتیبانی و پاسخگویی به سوالات با ما تماس بگیرید.</span>
        </div>
        <div className='mr-5'>
          <i className='fas fa-phone'></i>
          <span className='mr-1'>تلفن : 98765432 - 021</span>
        </div>
      </div>
      {/* FOOTER */}
      <Footer />
    </>
  );
}

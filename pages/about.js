import React from 'react';
import Link from 'next/link';

function about() {
  return (
    <div>
      <h1>about</h1>
      <Link href='/'>
        <a>link to homepage</a>
      </Link>
    </div>
  );
}

export async function getServerSideProps() {
  await new Promise((resolve) => {
    setTimeout(resolve, 2000);
  });
  return {
    props: {},
  };
}

export default about;

import { useState } from 'react';
import Head from 'next/head';
import Router from 'next/router';
import NProgress from 'nprogress';
import { ConfigProvider } from 'antd';
import AuthContext from '../context/Auth';
import 'antd/dist/antd.css';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import '../styles/globals.css';

Router.events.on('routeChangeStart', (url) => {
  console.log(`Loading: ${url}`);
  NProgress.start();
});
Router.events.on('routeChangeComplete', () => NProgress.done());
Router.events.on('routeChangeError', () => NProgress.done());

function MyApp({ Component, pageProps }) {
  const [auth, setAuth] = useState(false);

  const login = () => setAuth(true);

  const logout = () => setAuth(false);

  return (
    <ConfigProvider direction='rtl'>
      <AuthContext.Provider
        value={{ login: login, logout: logout, isAuthenticated: auth }}
      >
        <Head>
          <link rel='stylesheet' href='/nprogress.css' />
          <link rel='stylesheet' href='/css/bootstrap.min.css' />
          <link rel='stylesheet' href='/css/all.min.css' />
        </Head>
        <Component {...pageProps} />
      </AuthContext.Provider>
    </ConfigProvider>
  );
}

export default MyApp;

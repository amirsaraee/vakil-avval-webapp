import React from 'react';
import Head from 'next/head';
import CardSidebar from '../components/shared/CardSidebar';
import MainButton from '../components/UIElements/Button';
import Layout from '../components/shared/Layout';

function documentStudy() {
  return (
    <Layout>
      <Head>
        <title>صفحه مطالعه پرونده</title>
      </Head>
      <div class='QA-page text-right'>
        <div class='container'>
          <div class='row'>
            <div class='col-md-9'>
              <div class='QA-new-box'>
                <div class='QA-new-head'>
                  <img src='/img/Q&A-icon.png' alt='' />
                  <span>تقاضای مطالعه پرونده حقوقی</span>
                </div>
                <form action='' id='request-study-doc'>
                  <div class='row'>
                    <div class='col-md-12 mt-4'>
                      <div class='row'>
                        <div class='col-md-9'>
                          <div class='d-flex align-items-center'>
                            <div class='ml-3'>
                              <span>مشخصات متقاضی : </span>
                            </div>
                            <div>
                              <input
                                type='text'
                                placeholder='نام و نام خانوادگی'
                                class='form-control'
                              />
                            </div>
                            <div class='mr-3'>
                              <input
                                type='text'
                                placeholder='شماره تلفن همراه'
                                class='form-control'
                              />
                            </div>
                          </div>
                        </div>
                        <div class='col-md-3'>
                          <div class='d-flex pt-2'>
                            <span>جنسیت :</span>
                            <div class='mr-2'>
                              <input type='radio' name='gender' id='female' />
                              <label for='radio' className='mr-1'>
                                {' '}
                                خانم{' '}
                              </label>
                            </div>
                            <div class='mr-2'>
                              <input type='radio' name='gender' id='male' />
                              <label for='radio' className='mr-1'>
                                {' '}
                                آقا{' '}
                              </label>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class='col-md-12 mt-2 mb-4'>
                      <div class='pb-2'>
                        <div class='d-flex align-items-center'>
                          <div class='circle-icon'>
                            <i class='fas fa-bars'></i>
                          </div>
                          <span class='item-title mr-2'>شرح درخواست</span>
                          <span class='require-field'>* (اجباری)</span>
                        </div>
                      </div>
                      <div>
                        <textarea
                          name=''
                          id=''
                          rows='6'
                          minlength='50'
                          maxlength='400'
                          placeholder='درخواست یا مشکل حقوقی خود را به طور مختصر شرح دهید (حداقل 50 و حداکثر 400 کاراکتر)'
                          class='form-control request-text'
                        ></textarea>
                      </div>
                    </div>
                    <div class='col-md-12 mb-4 text-right'>
                      <div class='d-flex align-items-center mb-3'>
                        <div class='circle-icon'>
                          <i class='fas fa-link'></i>
                        </div>
                        <span class='item-title mr-2'>
                          ضمیمه کردن مستندات :
                        </span>
                      </div>
                      <div class='custom-file pb-5'>
                        <input
                          type='file'
                          class='custom-file-input'
                          id='customFile'
                        />
                        <label class='custom-file-label' for='customFile'>
                          + فایل مستندات و اوراق پرونده تان را جهت بررسی مشاور
                          بارگذاری کنید (حداکثر 30 فایل و حداکثر حجم فایل 500
                          کیلوبایت)
                        </label>
                      </div>
                      <span class='sub-input'>
                        * فرمت های مجاز فایل جهات بارگذاری : pdf - xls - xlsx -
                        zip - rar - doc - docx
                      </span>
                    </div>
                    <div class='col-md-12 text-right'>
                      <div class='request-conditon text-right'>
                        <div class='QA-form-alert'>
                          <i class='far fa-exclamation-circle ml-1'></i>
                          <span>ملاحظات :</span>
                        </div>
                        <ul>
                          <li>
                            1 - به منظور تسریع و تسهیل بارزگذاری فایلها، آنها را
                            داخل یک فولدر فشرده شده قرار داده و فایل زیپ را
                            ارسال نمایید.
                          </li>
                          <li>
                            2 - خدمات مطالاعات پرونده و هزینه آن ، شمال مطاله
                            فایل مستندات و اوراق ارسالی متقاضی ، تهیه گزارش
                            مختصر از پرونده و ارائه نظر مشورتی حقوقی به صورت
                            کتبی به متقاضی می باشد.
                          </li>
                          <li>
                            3 - مسئولیت مطالعه، گزارش و نظریه مشورتی صادره، از
                            هر حیث صرفا بر عهده مشاور حقوقی مربوطه می باشد.
                          </li>
                          <li>
                            4 - نتیجه کار ظرف 5 روز اداری از طریق ایمیل به
                            متقاضی اعلام خواهد شد
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div class='QA-from-action pb-3 pt-2'>
                    <div>
                      <div class='pb-2 price-item'>
                        <i class='far fa-usd-circle'></i>
                        <span class='item-title'>
                          هزینه خدمات : 200,000 تومان
                        </span>
                      </div>
                      <div>
                        <label for=''>
                          <input type='checkbox' />
                          <span>
                            <a href='' class='terms-conditions mr-1'>
                              شرایط و قوانین
                            </a>
                            مربوط به سایت وکیل اول را می پذیرم.
                          </span>
                        </label>
                      </div>
                    </div>
                    <div>
                      <button
                        type='button'
                        class='btn QA-form-btn'
                        data-toggle='modal'
                        data-target='.bd-example-modal-lg'
                      >
                        <span>ورود و ثبت سفارش</span>
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            <div class='col-md-3'>
              <div class='QA-sidebar'>
                <CardSidebar
                  title=' مشاوره تلفنی یا حضوری '
                  text=' برقراری تماس تلفنی یا رزرو ملاقات حضوری با وکیل جهت اخذ مشورت حقوقی '
                  image='contact-icon.png'
                >
                  <div>
                    <MainButton text='رزرو تلفنی' type='primary' />
                  </div>
                  <div className='mr-3'>
                    <MainButton text='رزرو ملاقات' type='primary' />
                  </div>
                </CardSidebar>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
}

export default documentStudy;

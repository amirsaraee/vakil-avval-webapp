import React from 'react';
import Head from 'next/head';
import Layout from '../components/shared/Layout';
import AddQuestion from '../components/Qusetion/AddQuestion';
import QuestionsList from '../components/Qusetion/QuestionsList';
import QuestionSidebar from '../components/Qusetion/QuestionSidebar';

function question() {
  return (
    <Layout>
      <Head>
        <title>ثبت پرسش</title>
      </Head>
      <div className='QA-page text-right'>
        <div className='container'>
          <div className='row'>
            <div className='col-md-9'>
              <AddQuestion />
              <QuestionsList />
            </div>
            <div className='col-md-3'>
              <QuestionSidebar />
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
}

export default question;

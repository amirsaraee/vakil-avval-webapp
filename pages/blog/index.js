import React from 'react';
import { Pagination } from 'antd';
import Layout from '../../components/shared/Layout';
import BlogPageTopItem from '../../components/Blog/BlogPageTopItem';
import SortFilter from '../../components/shared/SortFilter';
import BlogPageItem from '../../components/Blog/BlogPageItem';
import CardSidebar from '../../components/shared/CardSidebar';
import MainButton from '../../components/UIElements/Button';
import SidebarNewPosts from '../../components/Blog/SidebarNewPosts';
import SidebarImagePosts from '../../components/Blog/SidebarImagePosts';
import NewsLetter from '../../components/Blog/NewsLetter';

function BlogPage() {
  return (
    <Layout>
      <div class='blog-page text-right'>
        <div class='container'>
          <div class='row'>
            <div class='col-md-8'>
              <div class='contents-wrapper'>
                <BlogPageTopItem />
                <div class='more-content-wrap'>
                  <div class='more-content-head'>
                    <h4 class='item-title'>سایر مطالب وکیل اول</h4>
                    <div class='d-flex flex-wrap'>
                      <div class='ml-3'>
                        <div class='search-field'>
                          <i class='fal fa-search'></i>
                          <input type='text' placeholder='جستجو' />
                        </div>
                      </div>
                      <SortFilter />
                    </div>
                  </div>
                  <div class='more-content-box'>
                    <BlogPageItem />
                    <BlogPageItem />
                    <BlogPageItem />
                    <BlogPageItem />
                    <BlogPageItem />
                    <BlogPageItem />
                    <BlogPageItem />
                    <BlogPageItem />
                    <BlogPageItem />

                    <div class='Search-result-pagination'>
                      <Pagination defaultCurrent={1} total={50} />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class='col-md-4'>
              <CardSidebar
                title=' مشاوره تلفنی یا حضوری '
                text=' برقراری تماس تلفنی یا رزرو ملاقات حضوری با وکیل جهت اخذ مشورت حقوقی '
                image='contact-icon.png'
              >
                <div>
                  <MainButton text='رزرو تلفنی' type='primary' />
                </div>
                <div className='mr-3'>
                  <MainButton text='رزرو ملاقات' type='primary' />
                </div>
              </CardSidebar>
              <SidebarNewPosts />
              <SidebarImagePosts />
              <NewsLetter />
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
}

export default BlogPage;

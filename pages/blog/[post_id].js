import React from 'react';
import Layout from '../../components/shared/Layout';
import CardSidebar from '../../components/shared/CardSidebar';
import MainButton from '../../components/UIElements/Button';
import SidebarNewPosts from '../../components/Blog/SidebarNewPosts';
import SidebarImagePosts from '../../components/Blog/SidebarImagePosts';
import NewsLetter from '../../components/Blog/NewsLetter';
import PostItem from '../../components/Blog/PostItem';
import RelatedPost from '../../components/Blog/RelatedPost';

function BlogPost() {
  return (
    <Layout>
      <div class='blog-page text-right'>
        <div class='container'>
          <div class='row'>
            <div class='col-md-8'>
              <div class='contents-wrapper'>
                <PostItem />
                <div class='more-content-wrap'>
                  <div class='more-content-head'>
                    <div class='d-flex'>
                      <div class='star-rating'>
                        <span class='fas fa-star star-check'></span>
                        <span class='fas fa-star star-check'></span>
                        <span class='fas fa-star star-check'></span>
                        <span class='fas fa-star star-check'></span>
                        <span class='fas fa-star star-check'></span>
                      </div>
                      <span class='mr-2'>به این نوشته امتیاز دهید</span>
                    </div>
                    <div>
                      <button type='button' class='btn'>
                        <i class='fas fa-external-link-alt ml-1'></i>
                        <span>اشتراک گذاری</span>
                      </button>
                      <button type='button' class='btn'>
                        <i class='far fa-heart'></i>
                      </button>
                    </div>
                  </div>
                </div>
                <div class='post-review'>
                  <div class='pb-2'>
                    دیدگاه خود درباره مطلب فوق را با ما در میان بگذارید
                  </div>
                  <form action=''>
                    <textarea name='' id='' cols='30' rows='4'></textarea>
                    <div class='text-left pt-2'>
                      <button type='submit' class='btn btn-success'>
                        ارسال نظر
                      </button>
                    </div>
                  </form>
                </div>
                <div class='related-posts mt-4'>
                  <div class='blog-sidebar-head'>
                    <h4>
                      <span>نوشته های مرتبط</span>
                    </h4>
                  </div>
                  <div className='container'>
                    <div class='row w-100'>
                      <div class='col-md-4'>
                        <RelatedPost />
                      </div>
                      <div class='col-md-4'>
                        <RelatedPost />
                      </div>
                      <div class='col-md-4'>
                        <RelatedPost />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class='col-md-4'>
              <CardSidebar
                title=' مشاوره تلفنی یا حضوری '
                text=' برقراری تماس تلفنی یا رزرو ملاقات حضوری با وکیل جهت اخذ مشورت حقوقی '
                image='contact-icon.png'
              >
                <div>
                  <MainButton text='رزرو تلفنی' type='primary' />
                </div>
                <div className='mr-3'>
                  <MainButton text='رزرو ملاقات' type='primary' />
                </div>
              </CardSidebar>
              <SidebarNewPosts />
              <SidebarImagePosts />
              <NewsLetter />
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
}

export default BlogPost;

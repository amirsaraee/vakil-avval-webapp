import React from 'react';
import Head from 'next/head';
import Layout from '../../components/shared/Layout';
import LawyerInfo from '../../components/LawyerSingle/LawyerInfo';
import LawyerAddress from '../../components/LawyerSingle/LawyerAddress';
import AddComment from '../../components/LawyerSingle/AddComment';
import CommentAnalayse from '../../components/LawyerSingle/CommentAnalayse';
import CommenstList from '../../components/LawyerSingle/CommenstList';
import RelatedLawyers from '../../components/LawyerSingle/RelatedLawyers';
import SidebarCard from '../../components/LawyerSingle/SidebarCard';
import LawyerTabs from '../../components/LawyerSingle/LawyerTabs';

function laywerSinglePage() {
  return (
    <Layout>
      <Head>
        <title>صفحه مشاور تکی</title>
      </Head>
      <div class='single-wrapper'>
        <div class='container'>
          <div class='row'>
            <div class='col-md-7 text-right'>
              <div class='single-lawyer-box'>
                <LawyerInfo />

                <LawyerAddress />

                <LawyerTabs />

                <AddComment />

                <CommentAnalayse />

                <CommenstList />

                <RelatedLawyers />
              </div>
            </div>
            <div class='col-md-5 text-right'>
              <div class='single-lawyer-sidebar'>
                <SidebarCard title='مشاوره تلفنی' button='رزرو تماس'>
                  <span>تماس تلفنی با وکیل</span>
                  <span>حق المشاوره : 100,000 تومان به ازای هر 20 دقیقه</span>
                </SidebarCard>
                <SidebarCard title='مشاوره حضوری' button='رزرو ملاقات'>
                  <span>ملاقات حضوری با وکیل</span>
                  <span>
                    حق المشاوره : 300,000 تومان به ازای هر به 60 دقیقه
                  </span>
                </SidebarCard>
                <SidebarCard title='مطالعه پرونده' button='ثبت سفارش'>
                  <span>مطالعه پرونده، تهیه گزارش و تنظیم نظریه مشورتی</span>
                  <span>بهای خدمت : 4,000,000 تومان به طور مقطوع</span>
                </SidebarCard>
                <SidebarCard title='تنظیم اوراق ضقایی' button='ثبت سفارش'>
                  <span>بهای دادخواست : 4,000,000 تومان</span>
                  <span>بهای شکوائیه : 3,000,000 تومان</span>
                  <span>بهای لایحه : 4,500,000 تومان</span>
                  <span>بهای درخواست : 1,000,000 تومان</span>
                  <span>بهای اظهارنامه : 2,000,000 تومان</span>
                </SidebarCard>
                <SidebarCard title='تنظیم قرارداد' button='ثبت سفارش'>
                  <span>
                    بهای قرارداد داخلی - زبان فارسی : 50,000 تومان به ازای هر
                    صفحه
                  </span>
                  <span>
                    بهای قرارداد داخلی - زبان انگلیسی : 100,000 تومان به ازای هر
                    صفحه
                  </span>
                  <span>
                    بهای قرارداد داخلی - زبان فارسی و انگلیسی : 150,000 تومان به
                    ازای هر صفحه
                  </span>
                  <span>
                    بهای قرارداد بین المللی - زبان انگلیسی : 150,000 تومان به
                    ازای هر صفحه
                  </span>
                  <span>
                    بهای قرارداد بین المللی - زبان فارسی و انگلیسی : 200,000
                    تومان به ازای هر صفحه
                  </span>
                </SidebarCard>
                <SidebarCard title='بازبینی و اصلاح قرارداد' button='ثبت سفارش'>
                  <span>
                    بهای قرارداد داخلی - زبان فارسی : 50,000 تومان به ازای هر
                    صفحه
                  </span>
                  <span>
                    بهای قرارداد داخلی - زبان انگلیسی : 100,000 تومان به ازای هر
                    صفحه
                  </span>
                  <span>
                    بهای قرارداد داخلی - زبان فارسی و انگلیسی : 150,000 تومان به
                    ازای هر صفحه
                  </span>
                  <span>
                    بهای قرارداد بین المللی - زبان انگلیسی : 150,000 تومان به
                    ازای هر صفحه
                  </span>
                  <span>
                    بهای قرارداد بین المللی - زبان فارسی و انگلیسی : 200,000
                    تومان به ازای هر صفحه
                  </span>
                </SidebarCard>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
}

export default laywerSinglePage;

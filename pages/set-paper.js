import React from 'react';
import { Pagination } from 'antd';
import Layout from '../components/shared/Layout';
import PaperCard from '../components/SetPaperPage/PaperCard';
import Searchfilter from '../components/SetPaperPage/SearchFilter';
import PaperItem from '../components/SetPaperPage/PaperItem';

function setPaper() {
  return (
    <Layout>
      <div class='page-section text-right'>
        <div class='container'>
          <div class='card-section'>
            <span class='item-title'>
              تنظیم اوراق قضایی برحسب نیاز و بنا بر درخواست شما
            </span>
            <div class='card-box'>
              <PaperCard
                title='دادخواست'
                text='تنظیم متن دادخواست جهت ارائه به مرجع قضایی در دعاوی حقوقی مثل مطالبه وجه چک و الزام به تنظیم سند رسمی '
                price='30000'
              />
              <PaperCard
                title='شکواییه'
                text='تنظیم متن برگ شکوائیه جهت ارائه به مرجع قضایی در دعاوی کیفری مثل شکایت خیانت در امانت، کلاهبردای و فروش مال و غیره '
                price='20000'
              />
              <PaperCard
                title='لایحه'
                text='تنظیم متن هرگونه لایحه دفاعیه در دعاوی حقوقی و کیفری جهت ارائه به مرجع قضایی و درج در پرونده '
                price='150000'
              />
              <PaperCard
                title='درخواست'
                text='تنظیم متن هرگونه درخواست از مراجع قضایی جهت اقدام متخصص مانند درخواست صدور برگ اجرائیه، توقیف اموال محکوم نماید. '
                price='100000'
              />
              <PaperCard
                title='اظهارنامه'
                text='تنظیم متن هرگونه اظهارنامه جهت اظهار رسمی مطلب یا مطالبه حق یا درخواست اقدام لازم از سوی مخاطب اظهارنامه'
                price='10000'
              />
            </div>
          </div>
          <div class='select-lawyer'>
            <span class='item-title'>مشاهده و دانلود نمونه اوراق قضایی</span>
            <Searchfilter />
            <div class='paper-box'>
              <div class='row'>
                <div class='col-md-4'>
                  <PaperItem />
                </div>
                <div class='col-md-4'>
                  <PaperItem />
                </div>
                <div class='col-md-4'>
                  <PaperItem />
                </div>
                <div class='col-md-4'>
                  <PaperItem />
                </div>
                <div class='col-md-4'>
                  <PaperItem />
                </div>
                <div class='col-md-4'>
                  <PaperItem />
                </div>
                <div class='col-md-4'>
                  <PaperItem />
                </div>
                <div class='col-md-4'>
                  <PaperItem />
                </div>
                <div class='col-md-4'>
                  <PaperItem />
                </div>
              </div>
              <div class='Search-result-pagination'>
                <Pagination defaultCurrent={1} total={50} />
              </div>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
}

export default setPaper;

import React from 'react';
import Link from 'next/link';
import styles from './BlogCardItem.module.css';

function BlogCardItem(props) {
  return (
    <div className={styles.blog_item}>
      <div className={styles.blog_post_image}>
        <img src={`/img/${props.image}`} alt='' />
      </div>
      <div className={styles.blog_statistic}>
        <span>
          <i className='fas fa-calendar-alt ml-2'></i>
          {props.date}
        </span>
        <span>
          <i className='fas fa-eye ml-2'></i>
          {props.views}
        </span>
        <span>
          <i className='fas fa-comments ml-2'></i>
          {props.comments}
        </span>
      </div>
      <div className={styles.blog_item_content}>
        <Link href='/blog/[post_id]' as={`/blog/post`}>
          <a>
            <h3 className='text-center'>{props.title}</h3>
          </a>
        </Link>

        <p>{props.description}</p>
      </div>
    </div>
  );
}

export default BlogCardItem;

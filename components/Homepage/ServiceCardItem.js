import React from 'react';
import styles from './ServiceCardItem.module.css';
import Button from '../UIElements/Button';
import Link from 'next/link';

function ServiceCardItem(props) {
  return (
    <div className={styles.service_item}>
      <h4 className={styles.service_title}>{props.title}</h4>
      <div className={styles.service_icon}>
        <img src={`/img/${props.imageName}`} alt='' />
      </div>
      <div className={styles.service_desc}>
        <p>{props.text}</p>
      </div>
      <Button type='primary' text={props.linkName} url={props.url} />
    </div>
  );
}

export default ServiceCardItem;

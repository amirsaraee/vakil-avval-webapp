import React from 'react';
import Link from 'next/link';
import styles from './QuestionListItem.module.css';

function QuestionListItem(props) {
  return (
    <div className={styles.question_item}>
      <div className={styles.question_item_info}>
        <div className={styles.question_item_lawyer}>
          <div className='d-flex align-items-center'>
            <div>
              <img src={`/img/${props.image}`} alt='' />
            </div>
            <div className='mr-3'>
              <a href=''>
                <span className='d-block item-title'>{props.name}</span>
              </a>
              <span className='lawyer-expert'>{props.expert}</span>
            </div>
          </div>
        </div>
        <div className={styles.question_item_statistics}>
          <span>
            <i className='fas fa-calendar-alt ml-1'></i>
            {props.time}
          </span>
          <span>
            <i className='fas fa-comments ml-1'></i>
            {props.comments}
          </span>
          <span>
            <i className='fas fa-eye ml-1'></i>
            {props.views}
          </span>
        </div>
      </div>
      <div className={styles.question_item_content}>
        <div className={styles.item_question}>
          <span>پرسش : </span>
          <span>{props.question}</span>
        </div>
        <div>
          <span>پاسخ : </span>
          <span>{props.awnser} </span>
        </div>
      </div>
      <div className={styles.question_item_button}>
        <Link href='/question/[id]/awnsers' as={`/question/124/awnsers`}>
          <a href=''>
            <span>ادامه مطلب</span>
            <i className='fas fa-angle-left mr-1'></i>
          </a>
        </Link>
      </div>
    </div>
  );
}

export default QuestionListItem;

import React from 'react';
import Link from 'next/link';
import styles from './TopLawyerItem.module.css';

function TopLawyerItem(props) {
  return (
    <div className={styles.lawyer_item}>
      <div className={`${styles.lawyer_img} w-25`}>
        <img src={`/img/${props.image}`} alt='' />
      </div>
      <div className='mr-3 w-75'>
        <Link href='/lawyer/545'>
          <span className='d-block item-title'>{props.name}</span>
        </Link>
        <span>{props.expert}</span>
        <div className='item-statistic'>
          <div className='star-rating'>
            <span className='fas fa-star star-check'></span>
            <span className='fas fa-star star-check'></span>
            <span className='fas fa-star star-check'></span>
            <span className='fas fa-star star-check'></span>
            <span className='fas fa-star star-check'></span>
          </div>
          <div>
            <span>
              <span>{props.comments}</span>
              <i className='fas fa-comments mr-1'></i>
            </span>
            <span className='mr-2'>
              <span>{props.views}</span>
              <i className='fas fa-eye mr-1'></i>
            </span>
          </div>
        </div>
      </div>
    </div>
  );
}

export default TopLawyerItem;

import React from 'react';
import './HeaderHomePage.module.css';
import HeaderSearch from './HeaderSearch';
import HeaderShowcase from './HeaderShowcase';

function HeaderHomePgae() {
  return (
    <header className='header-section'>
      <div className='main-header'>
        <div className='container'>
          <div className='row align-items-center'>
            <div className='col-md-3 text-right'>
              <div className='logo'>
                <a href='index.html'>
                  <img src='/img/logo-2.png' alt='' />
                </a>
              </div>
            </div>
            <div className='col-md-6 text-right'>
              <HeaderSearch />
            </div>
          </div>
        </div>
      </div>
      <HeaderShowcase />
    </header>
  );
}

export default HeaderHomePgae;

import React, { useState } from 'react';
import { Modal, Steps, message, Button } from 'antd';
import Link from 'next/link';
import styles from './LawyerCardGridItem.module.css';
import MainButton from '../UIElements/Button';

const { Step } = Steps;

const steps = [
  {
    title: 'ارسال درخواست',
    content: (
      <fieldset>
        <div className='container'>
          <div className='row'>
            <div className='col-md-12'>
              <div className='row'>
                <div className='col-md-4'>
                  <div className='d-flex pt-2'>
                    <span>طرف مشاوره :</span>
                    <div className='mr-2'>
                      <input type='radio' name='consulting' />
                      <label for='radio' className='mr-1'>
                        خودم
                      </label>
                    </div>
                    <div className='mr-2'>
                      <input type='radio' name='consulting' />
                      <label for='radio' className='mr-1'>
                        شخص دیگر
                      </label>
                    </div>
                  </div>
                </div>
                <div className='col-md-3'>
                  <div className='d-flex pt-2'>
                    <span>جنسیت :</span>
                    <div className='mr-2'>
                      <input type='radio' name='gender' id='female' />
                      <label for='radio' className='mr-1'>
                        {' '}
                        خانم
                      </label>
                    </div>
                    <div className='mr-2'>
                      <input type='radio' name='gender' id='male' />
                      <label for='radio' className='mr-1'>
                        {' '}
                        آقا
                      </label>
                    </div>
                  </div>
                </div>
                <div className='col-md-5'>
                  <div className='d-flex'>
                    <div>
                      <input
                        type='text'
                        placeholder='نام و نام خانوادگی'
                        className='form-control'
                      />
                    </div>
                    <div className='mr-3'>
                      <input
                        type='text'
                        placeholder='شماره تلفن همراه'
                        className='form-control'
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className='col-md-12 mt-2 mb-2'>
              <div className='row'>
                <div className='col-md-4'>
                  <div className='d-flex'>
                    <span>نوع مشاوره :</span>
                    <div className='mr-2'>
                      <input type='radio' name='type-consult' />
                      <label for='radio' className='mr-1'>
                        تلفنی
                      </label>
                    </div>
                    <div className='mr-2'>
                      <input type='radio' name='type-consult' />
                      <label for='radio' className='mr-1'>
                        حضوری
                      </label>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className='col-md-12 mt-2 mb-4'>
              <div className='pb-2'>
                <div className='d-flex align-items-center'>
                  <div className='circle-icon'>
                    <i className='fas fa-bars'></i>
                  </div>
                  <span className='item-title mr-2'>شرح درخواست</span>
                  <span className='require-field'>* (اجباری)</span>
                </div>
              </div>
              <div>
                <textarea
                  name=''
                  id=''
                  rows='6'
                  minlength='50'
                  maxlength='400'
                  placeholder='درخواست یا مشکل حقوقی خود را به طور مختصر شرح دهید (حداقل 50 و حداکثر 400 کاراکتر)'
                  className='form-control request-text'
                ></textarea>
              </div>
            </div>
            <div className='col-md-12 mt-2 mb-4 text-right'>
              <div className='custom-file pb-5'>
                <input
                  type='file'
                  className='custom-file-input'
                  id='customFile'
                />
                <label className='custom-file-label' for='customFile'>
                  + مستندات خود را برای بررسی مشاور بارگذاری کنید (حداکثر 3 فایل
                  و حداکثر حجم فایل 2 مگابایت)
                </label>
              </div>
              <span className='sub-input'>
                * فرمت های مجاز فایل جهات بارگذاری : pdf - xls - xlsx - zip -
                rar - doc - docx
              </span>
            </div>
            <div className='col-md-12 text-right'>
              <div className='request-conditon text-right'>
                <div className='QA-form-alert'>
                  <i className='far fa-exclamation-circle ml-1'></i>
                  <span>ملاحظات مشاوره :</span>
                </div>
                <ul>
                  <li>
                    <i className='far fa-circle ml-1'></i>
                    با توجه به موضوع مشاوره و شرح مشکل خود، در انتخاب زمان و مدت
                    مشاوره دقت کنید.
                  </li>
                  <li>
                    <i className='far fa-circle ml-1'></i>
                    جهت اثر بخشی بیشتر مشاوره توصیه می شود فایل مستندات خود را
                    پیوست و در مشاوره حضوری به همراه داشته باشید.
                  </li>
                  <li>
                    <i className='far fa-circle ml-1'></i>
                    مسئولیت مشاوره ارائه شده از از هر حیث بر عهده مشاور حقوقی
                    مربوطه می باشد.
                  </li>
                  <li>
                    <i className='far fa-circle ml-1'></i>
                    سایر شرایط شرایط و مقررات مشاوره حقوقی بشرح مندرج در بخش
                    مربوطه از
                    <a href='' className='terms-conditions mr-1 ml-1'>
                      شرایط و قوانین
                    </a>
                    وب سایت می باشد.
                  </li>
                  <li>
                    <i className='far fa-circle ml-1'></i>
                    تماس تلفنی متقاضی با مشاور حقوقی و حضور در محل مشاوره حضوری،
                    صرفا در زمان و مدت تعیین شده امکانپذیر است.
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </fieldset>
    ),
  },
  {
    title: 'تکمیل و تایید اطلاعات',
    content: (
      <fieldset className='text-right'>
        <span className='item-title'>ثبت نام یا ورود کاربر</span>
        <div className='row mb-5'>
          <div className='col-md-4 col-12'>
            <label for=''>انتخاب کشور</label>
            <select name='' id='' className='custom-select'>
              <option value='iran'>ایران (98+)</option>
              <option value='usa'>امریکا (1+)</option>
            </select>
          </div>
          <div className='col-md-4 col-12'>
            <label for=''>ایمیل</label>
            <input
              type='text'
              placeholder='example@example.com'
              className='form-control text-left'
            />
          </div>
          <div className='col-md-4 col-12'>
            <label for=''>تلفن همراه</label>
            <input
              type='text'
              placeholder='9123456789'
              className='form-control text-left'
            />
          </div>
        </div>
      </fieldset>
    ),
  },
  {
    title: 'ثبت درخواست و پرداخت حق المشاوره',
    content: (
      <fieldset className='text-right'>
        <div>
          <div>
            <span>طرف مشاوره : </span>
            <span>آقای محمد محمدی ،</span>
            <span>تلفن : 9123446546</span>
          </div>
          <div>
            <span>مشاور حقوقی : </span>
            <span>آقای حسن حسنی ، وکیل پایه یک دادگستری</span>
          </div>
          <div className='d-flex flex-wrap mt-2 mb-2'>
            <span>نوع مشاوره : </span>
            <div className='mr-2'>
              <i className='fal fa-long-arrow-left ml-1'></i>
              <span>
                تلفنی - شماره تلفن مشاور جهت مشاوره : 979797 - 021 و 0915466464
              </span>
              <span className='d-block'>
                <i className='fal fa-long-arrow-left ml-1'></i>
                حضوری - محل مشاوره : تهران ونک ...
              </span>
            </div>
          </div>
          <div>
            <span>زمان مشاوره :</span>
            <span> چهارشنبه 29 تیر 1399 ساعت 18</span>
          </div>
          <div>
            <span>مدت و مبلغ مشاوره : </span>
            <span>40 دقیقه 120 هزار تومان</span>
          </div>

          <div className='pay-step mt-5 mb-5'>
            <div className='pay-staep-wrap'>
              <div>
                <span>پرداخت حق المشاوره</span>
              </div>
              <div className='pay-step-item money-bag'>
                <div>
                  <i className='far fa-sack-dollar ml-1'></i>
                  <span>کیف پول</span>
                </div>
                <div>
                  <span>موجودی : </span>
                  <span>0</span>
                </div>
              </div>
              <div className='pay-step-item code-discount'>
                <div className='discount-icon'>
                  <i className='fas fa-tags'></i>
                </div>
                <input type='text' placeholder='استفاده از کد تخفیف' />
                <div className='discount-button'>
                  <button type='button'>ثبت کد</button>
                </div>
              </div>
              <div className='pay-step-item select-pay'>
                <i className='far fa-credit-card'></i>
                <span className='mr-1'>انتخاب درگاه بانک عضو شتاب</span>
              </div>
              <div className='bank-btn-wrap'>
                <div className='bank-btn'>
                  <img src='/img/saman-logo.png' alt='' />
                  <input type='radio' name='bank' id='' />
                </div>
                <div className='bank-btn'>
                  <img src='/img/mellat.jpg' alt='' />
                  <input type='radio' name='bank' id='' />
                </div>
                <div className='bank-btn'>
                  <img src='/img/ap.png' alt='' />
                  <input type='radio' name='bank' />
                </div>
              </div>
              <div className='mt-5'>
                <button class='pay-btn'>پرداخت</button>
              </div>
            </div>
          </div>
        </div>
      </fieldset>
    ),
  },
];

function LawyerCardGridItem(props) {
  const [visible, setVisible] = useState(false);
  const [current, setCurrent] = useState(0);

  const next = () => {
    setCurrent(current + 1);
  };

  const prev = () => {
    setCurrent(current - 1);
  };

  const showModal = () => {
    setVisible(true);
  };

  const handleOk = (e) => {
    console.log(e);
    setVisible(false);
  };

  const handleCancel = (e) => {
    console.log(e);
    setVisible(false);
  };

  return (
    <div className='mx-2'>
      <div className={styles.lawyer_item}>
        <div className={styles.lawyer_item_inner}>
          <div className={styles.lawyer_img}>
            <img src={`/img/${props.image}`} alt='' />
          </div>
          <div className={styles.lawyer_item_info}>
            <Link href='/lawyer/545454'>
              <a>
                <span className='d-block mt-1 item-title'>{props.name}</span>
              </a>
            </Link>
            <span className='lawyer-expert'>{props.expert}</span>
          </div>

          <div className='item-statistic'>
            <div className='star-rating'>
              <span className='fas fa-star star-check'></span>
              <span className='fas fa-star star-check'></span>
              <span className='fas fa-star star-check'></span>
              <span className='fas fa-star'></span>
              <span className='fas fa-star'></span>
            </div>
            <div className='mr-3'>
              <span>
                <span>{props.comments}</span>
                <i className='fas fa-comments mr-1'></i>
              </span>
              <span className='mr-2'>
                <span>{props.views}</span>
                <i className='fas fa-eye mr-1'></i>
              </span>
            </div>
          </div>
        </div>
        <div>
          <div>
            <MainButton
              text='مشاوره تلفنی'
              type='primary'
              onClick={showModal}
            />
          </div>
          <div className='mt-2'>
            <MainButton
              text='مشاوره حضوری'
              type='primary'
              onClick={showModal}
            />
          </div>
        </div>
        <Modal
          title={
            (current === 0 &&
              ' جهت رزور مشاوره با وکیل مراحل زیر را دنبال کنید ') ||
            (current === 1 && ' تکمیل و تایید اطلاعات ') ||
            (current === 2 &&
              '   ملاحظه جزئیات - ثبت درخواست - پرداخت حق المشاوره   ')
          }
          visible={visible}
          onOk={handleOk}
          onCancel={handleCancel}
          width={1020}
          style={{ top: 20 }}
          okButtonProps={{ hidden: true }}
          cancelButtonProps={{ hidden: true }}
        >
          <Steps current={current}>
            {steps.map((item) => (
              <Step key={item.title} title={item.title} />
            ))}
          </Steps>
          <div className='steps-content'>{steps[current].content}</div>
          <div className='steps-action'>
            {current > 0 && (
              <Button style={{ margin: '0 8px' }} onClick={() => prev()}>
                مرحله قبلی
              </Button>
            )}
            {current < steps.length - 1 && (
              <Button type='primary' onClick={() => next()}>
                تایید و ورود به مرحله بعد
              </Button>
            )}
            {current === steps.length - 1 && (
              <Button
                type='primary'
                onClick={() => {
                  message.success('Processing complete!');
                  setVisible(false);
                }}
              >
                اتمام
              </Button>
            )}
          </div>
        </Modal>
      </div>
    </div>
  );
}

export default LawyerCardGridItem;

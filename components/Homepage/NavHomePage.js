import React from 'react';

import styles from './NavHomePage.module.css';
import MainNavigation from '../shared/MainNavigation';
import Auth from '../shared/Auth';

function NavHomePage(props) {
  return (
    <div className={styles.top_nav}>
      <div className='container'>
        <div className='row align-items-center flex-wrap'>
          <div className='col-md-9 text-center'>
            <MainNavigation />
          </div>
          <div className='col-md-3 text-left'>
            <div className='header-login'>
              {props.page ? (
                <a href='!#'>
                  <span>اخبار و پشتیبانی</span>
                </a>
              ) : (
                <Auth />
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default NavHomePage;

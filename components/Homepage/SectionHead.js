import React from 'react';
import Link from 'next/link';
import styles from './SectionHead.module.css';

function SectionHead(props) {
  return (
    <div className={styles.content_box}>
      <div className={styles.content_box_head}>
        <span className='item-title'>{props.title}</span>
        <Link href={`/${props.url}`} as={props.as}>
          <a>
            <span className='ml-1'>مشاهده همه</span>
            <i className='fas fa-angle-left'></i>
          </a>
        </Link>
      </div>
    </div>
  );
}

export default SectionHead;

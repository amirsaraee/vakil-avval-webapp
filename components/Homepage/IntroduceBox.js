import React, { useState } from 'react';
import { Modal, Cascader } from 'antd';
import MainButton from '../UIElements/Button';
import styles from './IntroduceBox.module.css';

const options = [
  {
    value: 'آذربایجان شرقی',
    label: 'آذربایجان شرقی',
    children: [
      {
        value: 'تبریز',
        label: 'تبریز',
      },
    ],
  },
  {
    value: 'تهران',
    label: 'تهران',
    children: [
      {
        value: 'تهران',
        label: 'تهران',
      },
      {
        value: 'پاکدشت',
        label: 'پاکدشت',
      },
    ],
  },
];

function IntroduceBox() {
  const [visible, setVisible] = useState(false);

  const showModal = () => {
    setVisible(true);
  };

  const handleOk = (e) => {
    console.log(e);
    setVisible(false);
  };

  const handleCancel = (e) => {
    console.log(e);
    setVisible(false);
  };

  return (
    <div className={styles.intro_box}>
      <div className='row align-items-center flex-wrap'>
        <div className='col-md-5 text-center'>
          <div className={styles.intro_lawyer}>
            <img src='/img/lawyer-intro.jpg' alt='' />
          </div>
        </div>
        <div className='col-md-7'>
          <div className='intro-text'>
            <h4>وکیل خوب میشناسید؟ به ما معرفی کنید.</h4>
            <p>
              با معرفی وکیل و سایر متخصصان حقوقی که تجربه خوبی از خدمات ارائه
              شده آنها دارید، این امکان را فراهم کنید که مخاطبان بیشتری از سراسر
              ایران و جهان با آنها و از خدمات آنها بهره مند شوند.
            </p>
            <div className='text-left'>
              <MainButton
                text='معرفی وکیل'
                type='primary'
                auto
                onClick={showModal}
              />
            </div>
            <Modal
              title='معرفی وکیل'
              visible={visible}
              onOk={handleOk}
              onCancel={handleCancel}
              style={{ top: 20 }}
              cancelButtonProps={{ hidden: true }}
              okButtonProps={{ hidden: true }}
              okText='تایید'
              cancelText='انصراف'
            >
              <form action='' class='modal-form-intro'>
                <div class='modal-input-box'>
                  <input type='text' placeholder='نام وکیل' />
                  <span class='modal-form-require-sign'>*</span>
                </div>
                <div class='modal-input-box'>
                  <input type='text' placeholder='نام خانوادگی وکیل' />
                  <span class='modal-form-require-sign'>*</span>
                </div>
                <div class='modal-input-box'>
                  <Cascader
                    options={options}
                    placeholder='لطفا انتخاب کنید'
                    style={{ width: '92%', marginLeft: '5px' }}
                  />
                  <span class='modal-form-require-sign'>*</span>
                </div>
                <div class='modal-input-box'>
                  <input type='text' placeholder='شماره تماس وکیل' />
                  <span class='modal-form-require-sign'>*</span>
                </div>
                <div class='modal-input-box'>
                  <textarea
                    name=''
                    id=''
                    rows='3'
                    placeholder='توضیحات'
                  ></textarea>
                </div>
                <div className='text-right'>
                  <div class='modal-form-warning'>
                    <i class='fas fa-circle ml-1'></i>
                    پر کردن فیلدهای ستاره دار الزامیست.
                  </div>
                  <div class='modal-form-warning'>
                    <i class='fas fa-circle ml-1'></i>
                    با تشکر از شما، همکاران ما بررسی و اقدامات لازم را انجام
                    خوهند داد.
                  </div>
                </div>
                <div class='mt-3'>
                  <button type='submit' class='modal-form-button'>
                    معرفی وکیل
                  </button>
                </div>
              </form>
            </Modal>
          </div>
        </div>
      </div>
    </div>
  );
}

export default IntroduceBox;

import React from 'react';
import Slider from 'react-slick';
import BlogCardItem from './BlogCardItem';

function BlogGrid() {
  const settings = {
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 3,
    draggable: true,
    infinite: false,
    rtl: true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 4,
          infinite: true,
          dots: true,
        },
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };
  return (
    <Slider {...settings}>
      <BlogCardItem
        image='lawyer-comment.jpg'
        title='10 نکته حقوقی جالب'
        description='لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپو با استفاده از طراحان گرافیک است'
        date='10 روز پیش'
        views='540'
        comments='10'
      />
      <BlogCardItem
        image='post-blog.jpg'
        title='10 نکته حقوقی جالب'
        description='لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپو با استفاده از طراحان گرافیک است'
        date='10 روز پیش'
        views='540'
        comments='10'
      />
      <BlogCardItem
        image='post-blog-2.jpg'
        title='10 نکته حقوقی جالب'
        description='لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپو با استفاده از طراحان گرافیک است'
        date='10 روز پیش'
        views='540'
        comments='10'
      />
      <BlogCardItem
        image='lawyer-comment.jpg'
        title='10 نکته حقوقی جالب'
        description='لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپو با استفاده از طراحان گرافیک است'
        date='10 روز پیش'
        views='540'
        comments='10'
      />
      <BlogCardItem
        image='post-blog.jpg'
        title='10 نکته حقوقی جالب'
        description='لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپو با استفاده از طراحان گرافیک است'
        date='10 روز پیش'
        views='540'
        comments='10'
      />
      <BlogCardItem
        image='post-blog-2.jpg'
        title='10 نکته حقوقی جالب'
        description='لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپو با استفاده از طراحان گرافیک است'
        date='10 روز پیش'
        views='540'
        comments='10'
      />
      <BlogCardItem
        image='post-blog-2.jpg'
        title='10 نکته حقوقی جالب'
        description='لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپو با استفاده از طراحان گرافیک است'
        date='10 روز پیش'
        views='540'
        comments='10'
      />
      <BlogCardItem
        image='post-blog-2.jpg'
        title='10 نکته حقوقی جالب'
        description='لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپو با استفاده از طراحان گرافیک است'
        date='10 روز پیش'
        views='540'
        comments='10'
      />
      <BlogCardItem
        image='post-blog-2.jpg'
        title='10 نکته حقوقی جالب'
        description='لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپو با استفاده از طراحان گرافیک است'
        date='10 روز پیش'
        views='540'
        comments='10'
      />
      <BlogCardItem
        image='post-blog-2.jpg'
        title='10 نکته حقوقی جالب'
        description='لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپو با استفاده از طراحان گرافیک است'
        date='10 روز پیش'
        views='540'
        comments='10'
      />
    </Slider>
  );
}

export default BlogGrid;

import React, { useState } from 'react';
import { Modal, Cascader } from 'antd';
import styles from './HeaderSearch.module.css';

const options = [
  {
    value: 'آذربایجان شرقی',
    label: 'آذربایجان شرقی',
    children: [
      {
        value: 'تبریز',
        label: 'تبریز',
      },
    ],
  },
  {
    value: 'تهران',
    label: 'تهران',
    children: [
      {
        value: 'تهران',
        label: 'تهران',
      },
      {
        value: 'پاکدشت',
        label: 'پاکدشت',
      },
    ],
  },
];

function HeaderSearch() {
  const [visible, setVisible] = useState(false);
  const [city, setCity] = useState('شهر');

  const showModal = () => {
    setVisible(true);
  };

  const handleOk = (e) => {
    console.log(e);
    setVisible(false);
  };

  const handleCancel = (e) => {
    console.log(e);
    setVisible(false);
  };

  const callbackHandler = (value, selectOptions) => {
    console.log(value, selectOptions);
    setCity(value[1]);
  };

  function filter(inputValue, path) {
    return path.some(
      (option) =>
        option.label.toLowerCase().indexOf(inputValue.toLowerCase()) > -1
    );
  }

  return (
    <form action=''>
      <div className={styles.search_form}>
        <button type='submit' className={styles.search_icon}>
          <i className='fas fa-search'></i>
        </button>
        <input type='text' placeholder='نام وکیل، تخصص حقوقی، نشانی دفتر ...' />
        <button type='button' className={styles.select_btn} onClick={showModal}>
          <i className='fas fa-map-marker-alt'></i>
          <span className='mr-1 ml-1'>{city}</span>
          <i className='fas fa-sort-down'></i>
        </button>

        <Modal
          title='انتخاب استان و شهر'
          visible={visible}
          okText='تایید'
          cancelText='انصراف'
          onOk={handleOk}
          onCancel={handleCancel}
        >
          <Cascader
            options={options}
            onChange={callbackHandler}
            placeholder='لطفا انتخاب کنید'
            showSearch={{ filter }}
          />
        </Modal>
      </div>
    </form>
  );
}

export default HeaderSearch;

import React from 'react';
import styles from './TopQuestionItem.module.css';
import Link from 'next/link';

function TopQuestionItem() {
  return (
    <div className={styles.top_question_item}>
      <div className={styles.question_item_lawyer}>
        <div className='d-flex align-items-center'>
          <div>
            <img src='/img/lawyer-3.jpg' alt='' />
          </div>
          <div className='mr-2 ml-3'>
            <Link href='/lawyer/4854'>
              <a>
                <span className='d-block item-title'>محمد محمدی</span>
              </a>
            </Link>
            <span className='lawyer-expert'>کارشناس ارشد حقوق جزا</span>
          </div>
          <div className={styles.question_item_statistics}>
            <span>
              <i className='fas fa-calendar-alt ml-1'></i>
              10 دقیقه پیش
            </span>
            <span>
              <i className='fas fa-comments ml-1'></i>4
            </span>
            <span>
              <i className='fas fa-eye ml-1'></i>
              20
            </span>
          </div>
        </div>
      </div>
      <div className='mt-3'>
        <span className='ml-2'>پرسش :</span>
        <Link href='/question/[id]/awnsers' as={`/question/124/awnsers`}>
          <a>
            <span>سوال درباره حقوق جزا و چگونگی آن</span>
          </a>
        </Link>
      </div>
    </div>
  );
}

export default TopQuestionItem;

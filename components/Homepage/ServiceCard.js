import React from 'react';
import Slider from 'react-slick';
import ServiceCardItem from './ServiceCardItem';

function ServiceCard(props) {
  const settings = {
    speed: 500,
    slidesToShow: 5,
    slidesToScroll: 5,
    draggable: true,
    rtl: true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 4,
          infinite: true,
          dots: true,
        },
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };

  return (
    <Slider {...settings}>
      <ServiceCardItem
        title='مشاوره تلفنی یا حضوری'
        imageName='contact-icon.png'
        text='رزرو تماس تلفنی یا ملاقات حضوری با وکیل جهت مشاوره'
        linkName='مشاهده وکلا برای مشاوره'
        url='/consult'
      />
      <ServiceCardItem
        title='پرسش و پاسخ حقوقی'
        imageName='Q&A-icon.png'
        text='طرح پرسش حقوقی به صورت آنلاین و دریافت پاسخ متخصصین حقوقی بطور سریع و رایگان'
        linkName='طرح پرسش'
        url='/question'
      />
      <ServiceCardItem
        title='مطالعه پرونده'
        imageName='doc-icon.png'
        text='مطالعه و بررسی پرونده حقوقی شما وو تهیه گزارش و تنظیم نظریه مشورتی'
        linkName='ارسال درخواست'
        url='/document-study'
      />
      <ServiceCardItem
        title='تنظیم اوراق قضایی'
        imageName='set-document-icon.png'
        text='تنظیم اوراق قاضیی مانند دادخواست، شکوائیه، اظهارنامه، لوایح دفاعی و غیره و دانلود نمونه های موجود'
        linkName='ارسال درخواست'
        url='/set-paper'
      />
      <ServiceCardItem
        title='تنظیم قرارداد'
        imageName='set-document-icon.png'
        text='تنظیم و اصلاح متون قراردادهای داخلی و بین المللی به زبان های مختلف و دانلود نمونه های موجود'
        linkName='ارسال درخواست'
        url='/set-contract'
      />
    </Slider>
  );
}

export default ServiceCard;

import React from 'react';
import styles from './IconItem.module.css';

function IconItem(props) {
  return (
    <div className={styles.icon_item}>
      <div className='logo-image text-center'>
        <img src={`/img/${props.icon}`} alt='' />
      </div>
      <p>{props.text}</p>
    </div>
  );
}

export default IconItem;

import React from 'react';
import styles from './HeaderShowcase.module.css';

function HeaderShowcase() {
  return (
    <div className='header-showcase'>
      <div className='container'>
        <div className='row'>
          <div className='col-md-7'>
            <div className={styles.header_text}>
              <p>
                1 - از میان{' '}
                <span className='blue-color'>وکلا و حقوق دانان</span>، متخصص
                حقوقی موردنظر خود را پیدا کنید
              </p>
              <p>
                2 - با توجه به زمینه فعالیت و{' '}
                <span className='blue-color'>تخصص های حقوقی</span> ، متخصص حقوقی
                موردنظرتان را بیابید.
              </p>
              <p>
                3 - براساس{' '}
                <span className='blue-color'>
                  موقعیت دفاتر حقوقی بر روی نقشه
                </span>
                ، متخصص حقوقی فعال در منطقه خود را پیدا کنید.
              </p>
            </div>
            <div className={styles.header_buttons}>
              <a href='' className={styles.header_btn}>
                <i className='far fa-user'></i>
                <span>حقوقدانان</span>
              </a>
              <a href='' className={styles.header_btn}>
                <i className='fas fa-balance-scale'></i>
                <span>تخصص ها</span>
              </a>
              <a href='' className={styles.header_btn}>
                <i className='fas fa-map-marker-alt'></i>
                <span>نشانی ها</span>
              </a>
            </div>
          </div>
          <div className='col-md-5'>
            <img src='/img/showcase.png' alt='' />
          </div>
        </div>
      </div>
    </div>
  );
}

export default HeaderShowcase;

import React from 'react';
import { Menu } from 'antd';
import Link from 'next/link';
import styles from './Sidebar.module.css';

function Sidebar(props) {
  return (
    <aside className={styles.main_sidebar}>
      <div className={styles.user_info}>
        <div className='d-flex justify-content-center align-items-center'>
          <div className={styles.user_avatar}>
            <i class='far fa-user'></i>
          </div>
        </div>
        <div className={styles.user_text}>
          <span className='d-block'>احمد محمدی</span>
          <span>09381234567</span>
        </div>
      </div>
      <Menu mode='inline' defaultSelectedKeys={[`${props.defaultKey}`]}>
        <Menu.Item key='1' icon={<i class='far fa-user-circle ml-1'></i>}>
          <Link href='/dashboard'>
            <a>اطلاعات کاربری</a>
          </Link>
        </Menu.Item>
        <Menu.Item key='2' icon={<i class='fas fa-gavel ml-2'></i>}>
          <Link href='/dashboard/services'>
            <a>خدمات حقوقی</a>
          </Link>
        </Menu.Item>
        <Menu.Item key='3' icon={<i class='fas fa-sign-out-alt ml-2'></i>}>
          خروج
        </Menu.Item>
      </Menu>
    </aside>
  );
}

export default Sidebar;

import React from 'react';
import { Collapse, Row, Col, Checkbox } from 'antd';
import styles from './LawServices.module.css';

const { Panel } = Collapse;

function LawServices() {
  return (
    <div>
      <div className={styles.section_title}>
        <i class='far fa-user-circle ml-1'></i>
        <span>خدمات حقوقی شما</span>
      </div>
      <Collapse accordion>
        <Panel header='انتخاب خدمات حقوقی' key='1'>
          <div>
            <div style={{ padding: ' 0 1.5rem' }}>
              <span>
                خدمات حقوقی که قادر به ارائه آن ها می باشید را انتخاب کنید.
              </span>
              <Checkbox.Group
                style={{
                  width: '100%',
                  textAlign: 'right',
                  marginTop: '1rem',
                  fontSize: '16px',
                }}
              >
                <Row>
                  <Col span={8} className='mb-2'>
                    <Checkbox value='A'>مشاوره حقوقی تلفنی</Checkbox>
                  </Col>
                  <Col span={8} className='mb-2'>
                    <Checkbox value='B'>مشاوره حقوقی حضوری</Checkbox>
                  </Col>
                  <Col span={8} className='mb-2'>
                    <Checkbox value='C'>مطالعه پرونده</Checkbox>
                  </Col>
                  <Col span={8} className='mb-2'>
                    <Checkbox value='D'>پاسخ به پرسش های حقوقی</Checkbox>
                  </Col>
                  <Col span={8} className='mb-2'>
                    <Checkbox value='E'>تنظیم اوراق قضایی</Checkbox>
                  </Col>
                  <Col span={8} className='mb-2'>
                    <Checkbox value='E'>
                      تنظیم / بازبینی قراردادهای داخلی
                    </Checkbox>
                  </Col>
                  <Col span={8} className='mb-2'>
                    <Checkbox value='E'>
                      تنظیم / بازبینی قراردادهای بین المللی
                    </Checkbox>
                  </Col>
                </Row>
              </Checkbox.Group>

              <div className={styles.form_warning}>
                <span>توجه : </span>
                خدمات مشاوره حقوقی تلفنی و حضوری و مطالعه پرونده صرفا توسط وکلا
                قابل ارائه و انتخاب می باشد.
              </div>
            </div>
          </div>
        </Panel>
        <Panel header='انتخاب زبان حقوقی حرفه ای' key='2'>
          <div>
            <div style={{ padding: ' 0 1.5rem' }}>
              <span>
                زبان یا زبان های حقوقی تخصصی و حرفه ای که توان و تمایل به ارائه
                خدمات با آنها را دارید را انتخاب نمایید.
              </span>
              <Checkbox.Group
                style={{
                  width: '100%',
                  textAlign: 'right',
                  marginTop: '1rem',
                  fontSize: '16px',
                }}
              >
                <Row>
                  <Col span={6} className='mb-2'>
                    <Checkbox value='A'>فارسی</Checkbox>
                  </Col>
                  <Col span={6} className='mb-2'>
                    <Checkbox value='B'>انگلیسی</Checkbox>
                  </Col>
                  <Col span={6} className='mb-2'>
                    <Checkbox value='C'>فرانسوی</Checkbox>
                  </Col>
                  <Col span={6} className='mb-2'>
                    <Checkbox value='D'>سایر زبان ها</Checkbox>
                  </Col>
                </Row>
              </Checkbox.Group>
            </div>
          </div>
        </Panel>
        <Panel header='انتخاب نوع، زمان، مدت مشاوره و مبلغ حق المشاوره' key='3'>
          <div>
            <div style={{ padding: ' 0 1.5rem' }}>
              <Row className={styles.form_box}>
                <Col span={5}>
                  <Checkbox value='1'>مشاوره تلفنی :</Checkbox>
                </Col>
                <Col span={19}>
                  <Row>
                    <Col span={12}>
                      <span>انتخاب تاریخ</span>
                    </Col>
                    <Col span={12}>
                      <span>انتخاب ساعت</span>
                    </Col>
                  </Row>
                  <Row style={{ marginTop: '4rem' }}>
                    <span>
                      انتخاب مدت مشاوره و مبلغ حق المشاوره (حداکثر 3 انتخاب و
                      حداکثر حق المشاوره 300/000 تومان)
                    </span>
                    <Col span={12}>
                      <div className={styles.checkbox_block}>
                        <Checkbox>10 دقیقه</Checkbox>
                        <input type='text' placeholder='مثلا 50000' />
                        <span>تومان</span>
                      </div>
                      <div className={styles.checkbox_block}>
                        <Checkbox>20 دقیقه</Checkbox>
                        <input type='text' placeholder='مثلا 100000' />
                        <span>تومان</span>
                      </div>
                      <div className={styles.checkbox_block}>
                        <Checkbox>30 دقیقه</Checkbox>
                        <input type='text' placeholder='مثلا 150000' />
                        <span>تومان</span>
                      </div>
                    </Col>
                    <Col span={12}>
                      <div className={styles.checkbox_block}>
                        <Checkbox>40 دقیقه</Checkbox>
                        <input type='text' placeholder='مثلا 200000' />
                        <span>تومان</span>
                      </div>
                      <div className={styles.checkbox_block}>
                        <Checkbox>50 دقیقه</Checkbox>
                        <input type='text' placeholder='مثلا 250000' />
                        <span>تومان</span>
                      </div>
                      <div className={styles.checkbox_block}>
                        <Checkbox>60 دقیقه</Checkbox>
                        <input type='text' placeholder='مثلا 300000' />
                        <span>تومان</span>
                      </div>
                    </Col>
                  </Row>
                </Col>
              </Row>
              <Row className={styles.form_box}>
                <Col span={5}>
                  <Checkbox value='1'>مشاوره حضوری :</Checkbox>
                </Col>
                <Col span={19}>
                  <Row>
                    <Col span={12}>
                      <span>انتخاب تاریخ</span>
                    </Col>
                    <Col span={12}>
                      <span>انتخاب ساعت</span>
                    </Col>
                  </Row>
                </Col>
                <div className={styles.form_warning}>
                  <span>توجه : </span>
                  مدت هر مشاوره حضوری حداکثر 60 دقیقه و مبلغ حق المشاوره حداکثر
                  300000 تومان می باشد.{' '}
                </div>
              </Row>
              <div className={styles.form_box}>
                <div style={{ marginBottom: '1rem' }}>
                  <Checkbox
                    style={{
                      fontWeight: 'bold',
                      marginBottom: '10px',
                    }}
                  >
                    تنظیم اوراق قضایی (مقطوعا و حداکثر 5,000,000 تومان)
                  </Checkbox>
                </div>
                <div className={styles.checkbox_wrap}>
                  <div className={styles.checkbox_block}>
                    <Checkbox>تنظیم دادخواست</Checkbox>
                    <input type='text' placeholder='مثلا 3,000,000' />
                    <span>تومان</span>
                  </div>
                  <div className={styles.checkbox_block}>
                    <Checkbox>تنظیم شکوائیه</Checkbox>
                    <input type='text' placeholder='مثلا 2,000,000' />
                    <span>تومان</span>
                  </div>
                  <div className={styles.checkbox_block}>
                    <Checkbox>تنظیم لایحه</Checkbox>
                    <input type='text' placeholder='مثلا 4,000,000' />
                    <span>تومان</span>
                  </div>
                  <div className={styles.checkbox_block}>
                    <Checkbox>تنظیم درخواست</Checkbox>
                    <input type='text' placeholder='مثلا 500,000' />
                    <span>تومان</span>
                  </div>
                  <div className={styles.checkbox_block}>
                    <Checkbox>تنظیم اظهارنامه</Checkbox>
                    <input type='text' placeholder='مثلا 1,500,000' />
                    <span>تومان</span>
                  </div>
                </div>
              </div>
              <div className={styles.form_box}>
                <div style={{ marginBottom: '1rem' }}>
                  <Checkbox className={styles.checkbox_title}>
                    مطالعه پرونده و تهیه گزارش و ضمیمه ضمیمه نظریه مشورتی
                    (مقطوعا و حداکثر 5,000,000 تومان)
                  </Checkbox>
                </div>
                <div className={styles.checkbox_wrap}>
                  <div className={styles.checkbox_block}>
                    <Checkbox>مطالعه پرونده حقوقی</Checkbox>
                    <input type='text' placeholder='مثلا 3,500,000' />
                    <span>تومان</span>
                  </div>
                  <div className={styles.checkbox_block}>
                    <Checkbox>مطالعه پرونده کیفری</Checkbox>
                    <input type='text' placeholder='مثلا 3,000,000' />
                    <span>تومان</span>
                  </div>
                  <div className={styles.checkbox_block}>
                    <Checkbox>مطالعه پرونده بین المللی</Checkbox>
                    <input type='text' placeholder='مثلا 5,000,000' />
                    <span>تومان</span>
                  </div>
                  <div className={styles.checkbox_block}>
                    <Checkbox>مطالعه پرونده اداری</Checkbox>
                    <input type='text' placeholder='مثلا 2,500,000' />
                    <span>تومان</span>
                  </div>
                  <div className={styles.checkbox_block}>
                    <Checkbox>سایر پرونده ها</Checkbox>
                    <input type='text' placeholder='مثلا 2,000,000' />
                    <span>تومان</span>
                  </div>
                </div>
              </div>
              <div className={styles.form_box}>
                <div>
                  <Checkbox>
                    تنظیم قراردادهای داخلی (تنظیم برای هر صفحه و حداکثر مبلغ هر
                    صفحه 600,000 تومان)
                  </Checkbox>
                </div>
                <div className={styles.checkbox_wrap}>
                  <Row>
                    <Col span={12}>
                      <div className={styles.checkbox_block}>
                        <Checkbox>فارسی</Checkbox>
                        <input type='text' placeholder='مثلا 150,000' />
                      </div>
                    </Col>
                    <Col span={12}>
                      <div className={styles.checkbox_block}>
                        <Checkbox>انگلیسی</Checkbox>
                        <input type='text' placeholder='مثلا 300,000' />
                      </div>
                    </Col>
                    <Col span={12}>
                      <div
                        className={`${styles.checkbox_block} ${styles.more_width}`}
                      >
                        <Checkbox>فارسی و انگلیسی (دوزبانه)</Checkbox>
                        <input type='text' placeholder='مثلا 400,000' />
                      </div>
                    </Col>
                    <Col span={12}>
                      <div className={styles.checkbox_block}>
                        <Checkbox>سایر زبان ها</Checkbox>
                        <input type='text' placeholder='مثلا 500,000' />
                      </div>
                    </Col>
                  </Row>
                </div>
              </div>
              <div className={styles.form_box}>
                <div>
                  <Checkbox>
                    بازبینی و اصلاح قراردادهای داخلی (مبلغ هر صفحه)
                  </Checkbox>
                </div>
                <div className={styles.checkbox_wrap}>
                  <Row>
                    <Col span={12}>
                      <div className={styles.checkbox_block}>
                        <Checkbox>فارسی</Checkbox>
                        <input type='text' placeholder='مثلا 100,000' />
                      </div>
                    </Col>
                    <Col span={12}>
                      <div className={styles.checkbox_block}>
                        <Checkbox>انگلیسی</Checkbox>
                        <input type='text' placeholder='مثلا 200,000' />
                      </div>
                    </Col>
                    <Col span={12}>
                      <div
                        className={`${styles.checkbox_block} ${styles.more_width}`}
                      >
                        <Checkbox>فارسی و انگلیسی (دوزبانه)</Checkbox>
                        <input type='text' placeholder='مثلا 300,000' />
                      </div>
                    </Col>
                    <Col span={12}>
                      <div className={styles.checkbox_block}>
                        <Checkbox>سایر زبان ها</Checkbox>
                        <input type='text' placeholder='مثلا 400,000' />
                      </div>
                    </Col>
                  </Row>
                </div>
              </div>
              <div className={styles.form_box}>
                <div>
                  <Checkbox>
                    تنظیم قرارادهای بین المللی (مبلغ هر صفحه){' '}
                  </Checkbox>
                </div>
                <div className={styles.checkbox_wrap}>
                  <Row>
                    <Col span={12}>
                      <div className={styles.checkbox_block}>
                        <Checkbox>فارسی</Checkbox>
                        <input type='text' placeholder='مثلا 300,000' />
                      </div>
                    </Col>
                    <Col span={12}>
                      <div className={styles.checkbox_block}>
                        <Checkbox>انگلیسی</Checkbox>
                        <input type='text' placeholder='مثلا 400,000' />
                      </div>
                    </Col>
                    <Col span={12}>
                      <div
                        className={`${styles.checkbox_block} ${styles.more_width}`}
                      >
                        <Checkbox>فارسی و انگلیسی (دوزبانه)</Checkbox>
                        <input type='text' placeholder='مثلا 500,000' />
                      </div>
                    </Col>
                    <Col span={12}>
                      <div className={styles.checkbox_block}>
                        <Checkbox>سایر زبان ها</Checkbox>
                        <input type='text' placeholder='مثلا 600,000' />
                      </div>
                    </Col>
                  </Row>
                </div>
              </div>
              <div className={styles.form_box}>
                <div>
                  <Checkbox>
                    بازبینی و اصلاح قراردادهای بین المللی (مبلغ هر صفحه)
                  </Checkbox>
                </div>
                <div className={styles.checkbox_wrap}>
                  <Row>
                    <Col span={12}>
                      <div className={styles.checkbox_block}>
                        <Checkbox>فارسی</Checkbox>
                        <input type='text' placeholder='مثلا 200,000' />
                      </div>
                    </Col>
                    <Col span={12}>
                      <div className={styles.checkbox_block}>
                        <Checkbox>انگلیسی</Checkbox>
                        <input type='text' placeholder='مثلا 300,000' />
                      </div>
                    </Col>
                    <Col span={12}>
                      <div
                        className={`${styles.checkbox_block} ${styles.more_width}`}
                      >
                        <Checkbox>فارسی و انگلیسی (دوزبانه)</Checkbox>
                        <input type='text' placeholder='مثلا 400,000' />
                      </div>
                    </Col>
                    <Col span={12}>
                      <div className={styles.checkbox_block}>
                        <Checkbox>سایر زبان ها</Checkbox>
                        <input type='text' placeholder='مثلا 500,000' />
                      </div>
                    </Col>
                  </Row>
                </div>
              </div>
            </div>
          </div>
        </Panel>
      </Collapse>
      <div className='mt-4 text-center'>
        <button className={styles.form_button}>ثبت تغییرات</button>
      </div>
    </div>
  );
}

export default LawServices;

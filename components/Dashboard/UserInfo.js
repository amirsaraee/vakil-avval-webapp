import React, { useState } from 'react';
import { Collapse, Radio, Select, Cascader } from 'antd';
import styles from './UserInfo.module.css';

const { Panel } = Collapse;
const { Option } = Select;

const options = [
  {
    value: 'تهران',
    label: 'تهران',
    children: [
      {
        value: 'پاکدشت',
        label: 'پاکدشت',
      },
      {
        value: 'شهر ری',
        label: 'شهر ری',
      },
    ],
  },
  {
    value: 'خراسان رضوی',
    label: 'خراسان رضوی',
    children: [
      {
        value: 'مشهد',
        label: 'مشهد',
      },
      {
        value: 'قوچان',
        label: 'قوچان',
      },
    ],
  },
];

function UserInfo() {
  const [activeMale, setActiveMale] = useState(false);
  const [activeFemale, setActiveFemale] = useState(false);

  const selectGenderHandler = (gender) => {
    if (gender === 'male') {
      setActiveMale(true);
      setActiveFemale(false);
    } else {
      setActiveFemale(true);
      setActiveMale(false);
    }
  };

  return (
    <div>
      <div className={styles.section_title}>
        <i class='far fa-user-circle ml-1'></i>
        <span>اطلاعات کاربری</span>
      </div>
      <Collapse accordion>
        <Panel
          header={
            <>
              <div className='d-flex align-items-center'>
                <span className={styles.collapse_icon}>
                  <i class='fas fa-bars'></i>
                </span>
                <span className={styles.collapse_text}>اطلاعات فردی</span>
              </div>
            </>
          }
          key='1'
        >
          <div>
            <div className={styles.form_wrap}>
              <div className={styles.form_block}>
                <div className={styles.form_label}>
                  <label htmlFor=''>نام کامل *</label>
                </div>
                <div className={styles.form_input}>
                  <div className={styles.input_groupe}>
                    <input
                      type='text'
                      className={styles.input_right}
                      placeholder='نام'
                    />
                  </div>
                  <div className={styles.input_groupe}>
                    <input
                      type='text'
                      className={styles.input_left}
                      placeholder='نام خانوادگی'
                    />
                  </div>
                </div>
              </div>
              <div className={styles.form_block}>
                <div className={styles.form_label}>
                  <label htmlFor=''>جنسیت *</label>
                </div>
                <div className={styles.form_input}>
                  <div
                    className={
                      activeMale
                        ? styles.radio_button_active
                        : styles.radio_button
                    }
                    style={{ borderRadius: '0 10px 10px 0' }}
                    onClick={() => selectGenderHandler('male')}
                  >
                    <i class='fal fa-male ml-1'></i>
                    <span>آقا</span>
                  </div>
                  <div
                    className={
                      activeFemale
                        ? styles.radio_button_active
                        : styles.radio_button
                    }
                    style={{ borderRadius: '10px 0 0 10px' }}
                    onClick={() => selectGenderHandler('female')}
                  >
                    <i class='fal fa-female ml-1'></i>
                    <span>خانم</span>
                  </div>
                </div>
              </div>
              <div className={styles.form_block}>
                <div className={styles.form_label}>
                  <label htmlFor=''>کد ملی *</label>
                </div>
                <div className={styles.form_input}>
                  <div className={styles.input_single}>
                    <input
                      type='text'
                      className={styles.input}
                      placeholder='مثلا 0912345678'
                    />
                  </div>
                </div>
              </div>
              <div className={styles.form_block}>
                <div className={styles.form_label}>
                  <label htmlFor=''>تصویر کارت ملی *</label>
                </div>
                <div className={styles.form_input}>
                  <div class='custom-file'>
                    <input
                      type='file'
                      class='custom-file-input'
                      id='customFile'
                    />
                    <label class='custom-file-label' for='customFile'>
                      فرمت pdf یا jpg با حداکثر سایز 500kb
                    </label>
                  </div>
                </div>
              </div>
              <div className={styles.form_block}>
                <div className={styles.form_label}>
                  <label htmlFor=''>تاریخ تولد *</label>
                </div>
              </div>
              <div className={styles.form_warning}>
                <span>توجه : </span>
                کد ملی و تصویر کارت ملی شما محرمانه محسوب و منتشر نمی شود.
              </div>
            </div>
          </div>
        </Panel>
        <Panel
          header={
            <>
              <div className='d-flex align-items-center'>
                <span className={styles.collapse_icon}>
                  <i class='fas fa-bars'></i>
                </span>
                <span className={styles.collapse_text}>اطلاعات حرفه ای</span>
              </div>
            </>
          }
          key='2'
        >
          <div className='user-form'>
            <div className={styles.form_wrap}>
              <div className={styles.form_block}>
                <div className={styles.form_label}>
                  <label htmlFor=''>شغل *</label>
                </div>
                <div className={styles.form_input}>
                  <Radio.Group className={styles.radio_box}>
                    <Radio value='1'>وکیل</Radio>
                    <Radio value='2'>کارشناس حقوقی</Radio>
                  </Radio.Group>
                </div>
              </div>
              <div className={styles.form_block}>
                <div className={styles.form_label}>
                  <label htmlFor=''>پایه وکالتی *</label>
                </div>
                <div className={styles.form_input}>
                  <Select
                    placeholder='انتخاب کنید'
                    style={{
                      width: '100%',
                    }}
                  >
                    <Option value='1'>وکیل پایه یک دادگستری</Option>
                    <Option value='2'>وکیل پایه یک قوه قضاییه</Option>
                  </Select>
                </div>
              </div>
              <div className={styles.form_block}>
                <div className={styles.form_label}>
                  <label htmlFor=''>مرجع صدور پروانه *</label>
                </div>
                <div className={styles.form_input}>
                  <input
                    type='text'
                    className={styles.input}
                    placeholder='مثلا کانون وکلای دادگستری مرکزی'
                  />
                </div>
              </div>
              <div className={styles.form_block}>
                <div className={styles.form_label}>
                  <label htmlFor=''>شماره پروانه *</label>
                </div>
                <div className={styles.form_input}>
                  <input
                    type='text'
                    className={styles.input}
                    placeholder='مثلا 1010/15'
                  />
                </div>
              </div>
              <div className={styles.form_block}>
                <div className={styles.form_label}>
                  <label htmlFor=''>تاریخ صدور پروانه *</label>
                </div>
                <div className={styles.form_input}></div>
              </div>
              <div className={styles.form_block}>
                <div className={styles.form_label}>
                  <label htmlFor=''>تاریخ اعتبار پروانه *</label>
                </div>
                <div className={styles.form_input}></div>
              </div>
              <div className={styles.form_block}>
                <div className={styles.form_label}>
                  <label htmlFor=''>شهر محل اشتغال (طبق پروانه) *</label>
                </div>
                <div className={styles.form_input}>
                  <Cascader
                    options={options}
                    placeholder='انتخاب استان و شهر'
                    style={{ width: '100%' }}
                  />
                </div>
              </div>
              <div className={styles.form_block}>
                <div className={styles.form_label}>
                  <label htmlFor=''>تصویر پروانه وکالت *</label>
                </div>
                <div className={styles.form_input}>
                  <div class='custom-file'>
                    <input
                      type='file'
                      class='custom-file-input'
                      id='customFile'
                    />
                    <label class='custom-file-label' for='customFile'>
                      فرمت pdf یا jpg با حداکثر سایز 500kb
                    </label>
                  </div>
                </div>
              </div>
              <div className={styles.form_warning}>
                <span>توجه : </span>
                هیچ یک از اطلاعات و مدارک حرفه ای شما به استثنای پایه وکالتی
                منتشر نخواهد شد.
              </div>
            </div>
          </div>
        </Panel>
        <Panel
          header={
            <>
              <div className='d-flex align-items-center'>
                <span className={styles.collapse_icon}>
                  <i class='fas fa-bars'></i>
                </span>
                <span className={styles.collapse_text}>
                  اطلاعات مکانی و تماس
                </span>
              </div>
            </>
          }
          key='3'
        >
          <div className='user-form'>
            <div className={styles.form_wrap}>
              <div className={styles.form_block}>
                <div className={styles.form_label}>
                  <label htmlFor=''>شماره موبایل *</label>
                </div>
                <div className={styles.form_input}>
                  <div className={styles.input_phone}>
                    <input
                      type='text'
                      className={`${styles.input} ${styles.input_ltr}`}
                    />
                    <span className={styles.pre_nubmrer}>+98</span>
                  </div>
                </div>
              </div>
              <div className={styles.form_block}>
                <div className={styles.form_label}>
                  <label htmlFor=''>شماره ثابت (تلفن) *</label>
                </div>
                <div className={styles.form_input}>
                  <div className={styles.input_phone}>
                    <input
                      type='text'
                      className={`${styles.input} ${styles.input_ltr}`}
                    />
                    <span className={styles.pre_nubmrer}>+98</span>
                  </div>
                </div>
              </div>
              <div className={styles.form_block}>
                <div className={styles.form_label}>
                  <label htmlFor=''>شماره ثابت (فکس)</label>
                </div>
                <div className={styles.form_input}>
                  <div className={styles.input_phone}>
                    <input
                      type='text'
                      className={`${styles.input} ${styles.input_ltr}`}
                    />
                    <span className={styles.pre_nubmrer}>+98</span>
                  </div>
                </div>
              </div>
              <div className={styles.form_block}>
                <div className={styles.form_label}>
                  <label htmlFor=''>محل فعالیت *</label>
                </div>
                <div className={styles.form_input}>
                  <Cascader
                    options={options}
                    placeholder='انتخاب استان و شهر'
                    style={{ width: '100%' }}
                  />
                </div>
              </div>
              <div className={styles.form_block}>
                <div className={styles.form_label}>
                  <label htmlFor=''>نشانی دفتر *</label>
                </div>
                <div className={styles.form_input}>
                  <textarea
                    name=''
                    id=''
                    cols='30'
                    rows='2'
                    className={styles.input}
                    placeholder='مثلا تهران، میدان ونک، خ ملاصدرا، پ 5، ط 4، و 24'
                  ></textarea>
                </div>
              </div>
              <div className={styles.form_block}>
                <div className={styles.form_label}>
                  <label htmlFor=''>انتخاب موقعیت دفتر بر روی نقشه *</label>
                </div>
              </div>
              <div className={styles.form_block}>
                <div className={styles.form_label}>
                  <label htmlFor=''>کد پستی </label>
                </div>
                <div className={styles.form_input}>
                  <input
                    type='text'
                    className={styles.input}
                    placeholder='مثلا 9451712345'
                  />
                </div>
              </div>
              <div className={styles.form_block}>
                <div className={styles.form_label}>
                  <label htmlFor=''>وب سایت شخصی </label>
                </div>
                <div className={styles.form_input}>
                  <input
                    type='text'
                    className={styles.input}
                    placeholder='مثلا http://www.yoursite.com'
                  />
                </div>
              </div>
              <div className={styles.form_block}>
                <div className={styles.form_label}>
                  <label htmlFor=''>ایمیل *</label>
                </div>
                <div className={styles.form_input}>
                  <input
                    type='text'
                    className={styles.input}
                    placeholder='مثلا you@example.com'
                  />
                </div>
              </div>
            </div>
          </div>
        </Panel>
        <Panel
          header={
            <>
              <div className='d-flex align-items-center'>
                <span className={styles.collapse_icon}>
                  <i class='fas fa-bars'></i>
                </span>
                <span className={styles.collapse_text}>سوابق کاری (مرتبط)</span>
              </div>
            </>
          }
          key='4'
        >
          <div className='user-form'>
            <div className={styles.form_wrap}>
              <div className={styles.form_block}>
                <div className={styles.form_label}>
                  <label htmlFor=''>محل کار</label>
                </div>
                <div className={styles.form_input}>
                  <input
                    type='text'
                    className={styles.input}
                    placeholder='مثلا شرکت لی پخش فرآورده های نفی ایران'
                  />
                </div>
              </div>

              <div className={styles.form_block}>
                <div className={styles.form_label}>
                  <label htmlFor=''>سمت</label>
                </div>
                <div className={styles.form_input}>
                  <input
                    type='text'
                    className={styles.input}
                    placeholder='مثلا کارشناس قراردادهای داخلی'
                  />
                </div>
              </div>
              <div className={styles.form_block}>
                <div className={styles.form_label}>
                  <label htmlFor=''>تاریخ شروع به کار</label>
                </div>
                <div className={styles.form_input}></div>
              </div>
              <div className={styles.form_block}>
                <div className={styles.form_label}>
                  <label htmlFor=''>تاریخ خاتمه کار</label>
                </div>
                <div className={styles.form_input}></div>
              </div>
              <div className='text-center my-3'>
                <button className={styles.add_button}>
                  + افزودن سابقه کاری
                </button>
              </div>
            </div>
          </div>
        </Panel>
        <Panel
          header={
            <>
              <div className='d-flex align-items-center'>
                <span className={styles.collapse_icon}>
                  <i class='fas fa-bars'></i>
                </span>
                <span className={styles.collapse_text}>
                  سوابق تحصیلی (مرتبط)
                </span>
              </div>
            </>
          }
          key='5'
        >
          <div className='user-form'>
            <div className={styles.form_wrap}>
              <div className={styles.form_block}>
                <div className={styles.form_label}>
                  <label htmlFor=''>رشته تحصیلی *</label>
                </div>
                <div className={styles.form_input}>
                  <input
                    type='text'
                    placeholder='مثلا حقوق قضایی'
                    className={styles.input}
                  />
                </div>
              </div>
              <div className={styles.form_block}>
                <div className={styles.form_label}>
                  <label htmlFor=''>مقطع تحصیلی *</label>
                </div>
                <div className={styles.form_input}>
                  <Select
                    placeholder='انتخاب کنید'
                    style={{
                      width: '100%',
                    }}
                  >
                    <Option value='1'>کارشناسی</Option>
                    <Option value='2'>کارشناسی ارشد</Option>
                    <Option value='3'>دکتری</Option>
                  </Select>
                </div>
              </div>
              <div className={styles.form_block}>
                <div className={styles.form_label}>
                  <label htmlFor=''>دانشگاه محل تحصیل *</label>
                </div>
                <div className={styles.form_input}>
                  <input
                    type='text'
                    placeholder='مثلا دانشگاه تهران'
                    className={styles.input}
                  />
                </div>
              </div>
              <div className={styles.form_block}>
                <div className={styles.form_label}>
                  <label htmlFor=''>تاریخ فراغت از تحصیل *</label>
                </div>
                <div className={styles.form_input}></div>
              </div>
              <div className={styles.form_block}>
                <div className={styles.form_label}>
                  <label htmlFor=''>تصویر مدرک تحصیلی *</label>
                </div>
                <div className={styles.form_input}>
                  <div class='custom-file'>
                    <input
                      type='file'
                      class='custom-file-input'
                      id='customFile'
                    />
                    <label class='custom-file-label' for='customFile'>
                      فرمت pdf یا jpg با حداکثر سایز 500kb
                    </label>
                  </div>
                </div>
              </div>
              <div className='text-center my-4'>
                <button className={styles.add_button}>
                  + افزودن سابقه تحصیلی
                </button>
              </div>
              <div className={styles.form_warning}>
                <span>توجه : </span>
                تصویر مدرک تحصیلی و اطلاعات تحصیلی به استثنای رشته و مقطع تحصیلی
                منتشر نمی شود.
              </div>
            </div>
          </div>
        </Panel>
        <Panel
          header={
            <>
              <div className='d-flex align-items-center'>
                <span className={styles.collapse_icon}>
                  <i class='fas fa-bars'></i>
                </span>
                <span className={styles.collapse_text}>
                  توضیحات متخصص حقوقی
                </span>
              </div>
            </>
          }
          key='6'
        >
          <div className='py-3'>
            <textarea
              name=''
              id=''
              cols='30'
              rows='7'
              placeholder='چنانچه توضیحاتی درباره موارد دیگری از قبیل پیام به کاربران، تخصص ها، دوره های آموزشی، گواهینامه ها و ... دارید، میتوانید در این قسمت درج نمایید.'
              className={styles.input}
            ></textarea>
          </div>
        </Panel>
      </Collapse>
      <div className='mt-4 text-center'>
        <button className={styles.form_button}>ثبت تغییرات</button>
      </div>
    </div>
  );
}

export default UserInfo;

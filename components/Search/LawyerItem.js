import React from 'react';
import Link from 'next/link';

function LawyerItem() {
  return (
    <div class='SRI'>
      <div class='row'>
        <div class='col-md-8'>
          <div class='SRI-lawyer-content'>
            <div class='SRI-lawyer-image text-center'>
              <div>
                <img src='/img/lawyer-1.jpg' alt='' />
              </div>
              <div class='d-flex justify-content-around align-items-center mt-3'>
                <div>
                  <span>56</span>
                  <i class='far fa-eye mr-1'></i>
                </div>
                <div>
                  <span class='lawyer-score'>10</span>
                </div>
              </div>
            </div>
            <div class='SRI-lawyer-info'>
              <div class='SRI-lawyer-title'>
                <i class='fas fa-badge-check'></i>
                <Link href='/lawyer/5445'>
                  <a>
                    <h3>وکیل محمد محمدی</h3>
                  </a>
                </Link>
              </div>
              <div class='SRI-lawyer-exprience pt-2'>
                <span>وکیل پایه یک دادگستری</span>
              </div>
              <div class='SRI-lawyer-statis pt-2'>
                <div class='star-rating'>
                  <span class='fas fa-star star-check'></span>
                  <span class='fas fa-star star-check'></span>
                  <span class='fas fa-star star-check'></span>
                  <span class='fas fa-star'></span>
                  <span class='fas fa-star'></span>
                </div>
                <span class='mr-5'>
                  2100
                  <i class='fal fa-user mr-1'></i>
                </span>
                <a href='' class='mr-4 SRI-lawyer-feature'>
                  <i class='far fa-credit-card ml-1'></i>
                  <span>پرداخت آنلاین</span>
                </a>
              </div>
              <div class='SRI-lawyer-address pt-2'>
                <span class='ml-2'>نشانی دفتر : </span>
                <p>
                  وکیل آباد، بعد از بلوار دانش آموز، پلاک ۶۰۳، ساختمان پزشکان
                  دانش آموز
                </p>
              </div>
              <div class='SRI-view-link'>
                <a href=''>
                  <span>مشاهده پروفایل</span>
                  <i class='far fa-angle-left mr-2'></i>
                </a>
              </div>
            </div>
          </div>
        </div>
        <div class='col-md-4'>
          <div class='SRI-reserv-info'>
            <div>
              <div>
                <span class='item-title'>خدمات متخصص</span>
              </div>
              <ul>
                <li class='expert-services-item'>
                  <i class='far fa-check-square'></i>
                  <span>مشاوره تلفنی</span>
                </li>
                <li class='expert-services-item'>
                  <i class='far fa-check-square'></i>
                  <span>مشاوره حضوری</span>
                </li>
                <li class='expert-services-item'>
                  <i class='far fa-check-square'></i>
                  <span>مطالعه پرونده</span>
                </li>
                <li class='expert-services-item'>
                  <i class='far fa-check-square'></i>
                  <span>تنظیم اوراق قضایی</span>
                </li>
                <li class='expert-services-item'>
                  <i class='far fa-check-square'></i>
                  <span>تنظیم اوراق قضایی</span>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default LawyerItem;

import React from 'react';
import { Pagination } from 'antd';
import LawyerItem from './LawyerItem';

function LawyersList() {
  return (
    <div class='search-result-box'>
      <LawyerItem />
      <LawyerItem />
      <LawyerItem />
      <LawyerItem />
      <LawyerItem />
      <LawyerItem />

      <div class='Search-result-pagination'>
        <Pagination defaultCurrent={1} total={50} />
      </div>
    </div>
  );
}

export default LawyersList;

import React, { useState } from 'react';
import { Modal, Checkbox } from 'antd';
import SortFilter from '../shared/SortFilter';

function SearchFilter() {
  const [visible, setVisible] = useState(false);

  const showModal = () => {
    setVisible(true);
  };

  const handleOk = (e) => {
    console.log(e);
    setVisible(false);
  };

  const handleCancel = (e) => {
    setVisible(false);
  };

  return (
    <div class='search-filter-wrap'>
      <div class='filter-box'>
        <button type='button' class='filter-btn' onClick={showModal}>
          <i class='far fa-filter'></i>
          <span>فیلترها</span>
        </button>

        <Modal
          title='فیلتر ها'
          visible={visible}
          onOk={handleOk}
          onCancel={handleCancel}
          style={{ top: 20 }}
        >
          <div>
            <div class='modal-body-box'>
              <div class='modal-body-head'>
                <i class='far fa-id-card ml-2'></i>
                <span>خدمات حقوقی</span>
              </div>
              <div class='modal-body-content'>
                <div class='modal-checkboxes'>
                  <div class='modal-body-checkbox'>
                    <Checkbox>مشاوره حقوقی</Checkbox>
                  </div>
                  <div class='modal-body-checkbox'>
                    <Checkbox>مشاوره حقوقی</Checkbox>
                  </div>
                  <div class='modal-body-checkbox'>
                    <Checkbox>مشاوره حقوقی</Checkbox>
                  </div>
                  <div class='modal-body-checkbox'>
                    <Checkbox>مشاوره حقوقی</Checkbox>
                  </div>
                  <div class='modal-body-checkbox'>
                    <Checkbox>مشاوره حقوقی</Checkbox>
                  </div>
                </div>
              </div>
            </div>
            <div class='modal-body-box'>
              <div class='modal-body-head'>
                <i class='far fa-check-square ml-2'></i>
                <span>سمت متخصص</span>
              </div>
              <div class='modal-body-content'>
                <div class='modal-checkboxes'>
                  <div class='modal-body-checkbox'>
                    <Checkbox>مشاوره حقوقی</Checkbox>
                  </div>
                  <div class='modal-body-checkbox'>
                    <Checkbox>مشاوره حقوقی</Checkbox>
                  </div>
                  <div class='modal-body-checkbox'>
                    <Checkbox>مشاوره حقوقی</Checkbox>
                  </div>
                  <div class='modal-body-checkbox'>
                    <Checkbox>مشاوره حقوقی</Checkbox>
                  </div>
                  <div class='modal-body-checkbox'>
                    <Checkbox>مشاوره حقوقی</Checkbox>
                  </div>
                  <div class='modal-body-checkbox'>
                    <Checkbox>مشاوره حقوقی</Checkbox>
                  </div>
                </div>
              </div>
            </div>
            <div class='modal-body-box'>
              <div class='modal-body-head'>
                <i class='far fa-check-square ml-2'></i>
                <span>زمینه فعالیت</span>
              </div>
              <div class='modal-body-content'>
                <div class='modal-checkboxes'>
                  <div class='modal-body-checkbox'>
                    <Checkbox>مشاوره حقوقی</Checkbox>
                  </div>
                  <div class='modal-body-checkbox'>
                    <Checkbox>مشاوره حقوقی</Checkbox>
                  </div>
                  <div class='modal-body-checkbox'>
                    <Checkbox>مشاوره حقوقی</Checkbox>
                  </div>
                  <div class='modal-body-checkbox'>
                    <Checkbox>مشاوره حقوقی</Checkbox>
                  </div>
                  <div class='modal-body-checkbox'>
                    <Checkbox>مشاوره حقوقی</Checkbox>
                  </div>
                  <div class='modal-body-checkbox'>
                    <Checkbox>مشاوره حقوقی</Checkbox>
                  </div>
                </div>
              </div>
            </div>
            <div class='modal-body-box'>
              <div class='modal-body-head'>
                <i class='far fa-check-square ml-2'></i>
                <span>جنسیت متخصص</span>
              </div>
              <div class='modal-body-content'>
                <div class='modal-checkboxes'>
                  <div class='modal-body-checkbox'>
                    <Checkbox>مشاوره حقوقی</Checkbox>
                  </div>
                  <div class='modal-body-checkbox'>
                    <Checkbox>مشاوره حقوقی</Checkbox>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Modal>
      </div>
      <SortFilter />
      <div class='mr-2'>
        <div class='search-field'>
          <i class='fal fa-search'></i>
          <input type='text' placeholder='نام متخصص' />
        </div>
      </div>
    </div>
  );
}

export default SearchFilter;

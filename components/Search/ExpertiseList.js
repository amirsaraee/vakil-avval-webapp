import React from 'react';
import ExpertiseItem from './ExpertiseItem';

function ExpertiseList() {
  return (
    <div class='experiences-list'>
      <ExpertiseItem />
      <ExpertiseItem />
      <ExpertiseItem />
      <ExpertiseItem />
      <ExpertiseItem />
    </div>
  );
}

export default ExpertiseList;

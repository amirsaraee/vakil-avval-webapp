import React from 'react';

function ExpertiseItem() {
  return (
    <div class='experience-item'>
      <div class='row'>
        <div class='col-md-8'>
          <div>
            <div class='exp-title'>
              <i class='far fa-check-square'></i>
              <h3>دعاوی حقوقی</h3>
            </div>
            <div class='exp-description'>
              <p>
                دعاوی حقوقی مستقیماٌ در محاکم دادگستری مطرح می شود و دیگر نیازی
                نیست که به دادسرای عمومی و انقلاب مراجعه کرد بلکه بدواٌ در
                دادگاه مطرح و مورد رسیدگی قرار می گیرد.
              </p>
            </div>
          </div>
        </div>
        <div class='col-md-4'>
          <div class='text-center'>
            <div class='circle-images-lawyers pt-3'>
              <div class='CIL-more-items'>
                <span>+20</span>
              </div>
              <img src='/img/lawyer-1.jpg' alt='' />
              <img src='/img/lawyer-2.jpg' alt='' />
              <img src='/img/lawyer-3.jpg' alt='' />
            </div>
            <div class='mt-3'>
              <a href='' class='service-btn text-center w-50 mr-auto ml-auto'>
                مشاهده وکلا و کارشناسان
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ExpertiseItem;

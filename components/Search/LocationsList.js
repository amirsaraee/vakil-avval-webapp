import React from 'react';
import LocationItem from './LocationItem';

function LocationsList() {
  return (
    <div class='experiences-list'>
      <LocationItem />
      <LocationItem />
      <LocationItem />
      <LocationItem />
      <LocationItem />
      <LocationItem />
    </div>
  );
}

export default LocationsList;

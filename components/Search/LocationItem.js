import React from 'react';

function LocationItem() {
  return (
    <div class='experience-item'>
      <div class='row'>
        <div class='col-md-8'>
          <div class='location-item-info'>
            <div class='location-title'>
              <i class='fal fa-map-marker-alt'></i>
              <div class='mr-2'>
                <h3>دفتر حقوقی</h3>
                <span>تهران - ونک</span>
              </div>
            </div>
          </div>
        </div>
        <div class='col-md-4'>
          <div class='text-center'>
            <div class='circle-images-lawyers pt-3'>
              <div class='CIL-more-items'>
                <span>+17</span>
              </div>
              <img src='/img/lawyer-1.jpg' alt='' />
              <img src='/img/lawyer-2.jpg' alt='' />
              <img src='/img/lawyer-3.jpg' alt='' />
            </div>
            <div class='mt-3'>
              <a href='' class='service-btn text-center w-50 mr-auto ml-auto'>
                مشاهده وکلا و کارشناسان
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default LocationItem;

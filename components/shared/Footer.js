import React from 'react';
import styles from './Footer.module.css';

function Footer() {
  return (
    <footer className={`${styles.main_footer} text-right`}>
      <div className='container'>
        <div className='row pb-3'>
          <div className='col-md-3'>
            <div className={styles.footer_item}>
              <div className={styles.footer_head}>
                <h4>وکیل اول</h4>
              </div>
              <div className={styles.footer_content}>
                <ul className={styles.footer_list}>
                  <li>
                    <a href=''>درباره وکیل اول</a>
                  </li>
                  <li>
                    <a href=''>مطالب حقوقی</a>
                  </li>
                  <li>
                    <a href=''>شرایط استفاده</a>
                  </li>
                  <li>
                    <a href=''>حریم خصوصی</a>
                  </li>
                  <li>
                    <a href=''>ارسال بازخورد</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div className='col-md-3'>
            <div className={styles.footer_item}>
              <div className={styles.footer_head}>
                <h4>برای مراجعین</h4>
              </div>
              <div className={styles.footer_content}>
                <ul className={styles.footer_list}>
                  <li>
                    <a href=''>پرسش های متداول</a>
                  </li>
                  <li>
                    <a href=''>ورود و عضویت</a>
                  </li>
                  <li>
                    <a href=''>رزرو مشاوره</a>
                  </li>
                  <li>
                    <a href=''>پرسش های حقوقی</a>
                  </li>
                  <li>
                    <a href=''>معرفی وکیل</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div className='col-md-3'>
            <div className={styles.footer_item}>
              <div className={styles.footer_head}>
                <h4>برای وکلا و حقوقدانان</h4>
              </div>
              <div className={styles.footer_content}>
                <ul className={styles.footer_list}>
                  <li>
                    <a href=''>پرسش های متداول</a>
                  </li>
                  <li>
                    <a href=''>ورود و عضویت</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div className='col-md-3'>
            <div className={styles.footer_item}>
              <div className={`${styles.footer_head} text-center`}>
                <h4>گواهی نامه ها</h4>
              </div>
              <div className={`${styles.footer_content} ${styles.namad}`}>
                <div className='d-flex justify-content-around m-3'>
                  <img src='/img/national.1e9150ba.png' alt='' />
                  <img src='/img/organization.9a07b3fb.png' alt='' />
                </div>
                <div className='d-flex justify-content-around m-3'>
                  <img src='/img/Subtraction1.8389966b.png' alt='' />
                  <img src='/img/virtual.b327b9eb.png' alt='' />
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className={styles.copyright}>
          <p>کلیه حقوق این سایت متعلق به وب سایت وکیل اول می باشد</p>
          <div className={styles.social_media}>
            <span>وکیل اول در شبکه های اجتماعی</span>
            <div className='mr-4'>
              <a href=''>
                <i className='fab fa-instagram'></i>
              </a>
              <a href=''>
                <i className='fab fa-telegram'></i>
              </a>
              <a href=''>
                <i className='fab fa-linkedin-in'></i>
              </a>
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
}

export default Footer;

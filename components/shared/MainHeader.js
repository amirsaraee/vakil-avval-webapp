import React from 'react';
import Link from 'next/link';
import NavHomePage from '../Homepage/NavHomePage';
import HeaderSearch from '../Homepage/HeaderSearch';
import Auth from './Auth';

function MainHeader() {
  return (
    <>
      <NavHomePage page={true} />
      <div className='header-section page-header'>
        <div className='main-header'>
          <div className='container'>
            <div className='row align-items-center'>
              <div className='col-md-3 text-right'>
                <div className='logo'>
                  <Link href='/'>
                    <a>
                      <img src='/img/logo-2.png' alt='' />
                    </a>
                  </Link>
                </div>
              </div>
              <div className='col-md-6 text-right'>
                <HeaderSearch />
              </div>
              <div className='col-md-3 text-left'>
                <div className='header-login'>
                  <Auth />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default MainHeader;

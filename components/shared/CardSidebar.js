import React from 'react';

function CardSidebar(props) {
  return (
    <div className='QA-sidebar-card'>
      <div className='QA-sidebar-title text-center'>
        <span className='item-title'>{props.title}</span>
      </div>
      <div className='QA-sidebar-icon'>
        <img src={`/img/${props.image}`} alt='' />
      </div>
      <div className='QA-sidebar-text'>
        <span>{props.text} </span>
      </div>
      <div className='QA-sidebar-actions'>{props.children}</div>
    </div>
  );
}

export default CardSidebar;

import React from 'react';
import MainHeader from './MainHeader';
import Footer from './Footer';

function Layout(props) {
  return (
    <>
      <MainHeader />
      {props.children}
      <Footer />
    </>
  );
}

export default Layout;

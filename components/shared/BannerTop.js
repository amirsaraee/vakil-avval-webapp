import React from 'react';
import styles from './BannerTop.module.css';

function BannerTop() {
  return (
    <div className='top-banner'>
      <div className={styles.banner_box}>
        <span>به صورت تلفنی و فورا با وکیل مشورت کنید</span>
        <a
          href='!#'
          className='btn bg-red mr-3'
          data-toggle='modal'
          data-target='.bd-example-modal-lg'
        >
          <span>تماس فوری</span>
        </a>
      </div>
    </div>
  );
}

export default BannerTop;

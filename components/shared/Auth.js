import React, { useState, useEffect, useContext } from 'react';
import {
  Modal,
  Form,
  Input,
  Radio,
  Checkbox,
  Row,
  Col,
  Menu,
  Dropdown,
  Button,
} from 'antd';
import ReactCodeInput from 'react-verification-code-input';
import { useRouter } from 'next/router';
import Link from 'next/link';
import AuthContext from '../../context/Auth';
import styles from './Auth.module.css';

function Auth() {
  const [visible, setVisible] = useState(false);
  const [current, setCurrent] = useState(0);
  const [phone, setPhone] = useState(null);
  const [count, setCount] = useState(60);
  const [radioValue, setRadioValue] = useState(null);
  const router = useRouter();
  const Auth = useContext(AuthContext);

  const menu = (
    <Menu>
      <Menu.Item key='0'>
        <Link href='/dashboard'>
          <a>پنل کاربری</a>
        </Link>
      </Menu.Item>
      <Menu.Item
        key='1'
        onClick={() => {
          Auth.logout();
          router.push('/');
        }}
      >
        خروج
      </Menu.Item>
    </Menu>
  );

  useEffect(() => {
    count > 0 && setTimeout(() => setCount(count - 1), 1000);
  }, [count]);

  const showModal = () => {
    setVisible(true);
  };

  const okHandler = () => {
    setVisible(false);
  };

  const cancelHandler = () => {
    setVisible(false);
    setCurrent(0);
    setCount(60);
    setRadioValue(null);
  };

  const next = () => {
    setCurrent(current + 1);
  };

  const prev = () => {
    setCurrent(current - 1);
  };

  const steps = [
    {
      title: 'ورود شماره موبایل',
      content: (
        <div>
          <span>برای ورود به وکیل اول ابتدا شماره موبایل خود را وارد کنید</span>

          <Form style={{ width: '70%', margin: '1rem auto', direction: 'ltr' }}>
            <Form.Item
              name='phone'
              rules={[
                { required: true, message: 'Please input your phone number!' },
              ]}
              style={{ direction: 'ltr' }}
            >
              <Input
                addonBefore='+98'
                style={{ width: '100%', direction: 'ltr' }}
                onChange={(e) => setPhone(e.target.value)}
              />
            </Form.Item>

            <button type='button' onClick={next} className={styles.step_button}>
              مرحله بعد
            </button>
          </Form>
        </div>
      ),
    },
    {
      title: 'تایید هویت',
      content: (
        <div style={{ width: '90%' }}>
          <span>کد تایید هویت ارسالی به شما را وارد کنید.</span>
          <div className={styles.verify_info}>
            <div>
              <span>شماره موبایل :</span>
              <span className={styles.phone}> {phone}</span>
            </div>

            <a onClick={prev} className={styles.edit}>
              ویرایش <i className='fas fa-angle-left'></i>
            </a>
          </div>

          <ReactCodeInput
            fields={5}
            className={styles.verify_box}
            onComplete={() => {
              setCurrent(current + 1);
            }}
          />
          {count === 0 && (
            <div className='mt-3'>
              <a onClick={next} className={styles.resend_code}>
                <i class='far fa-envelope ml-1'></i>
                <span>ارسال مجدد کد</span>
              </a>
            </div>
          )}
          {count !== 0 && (
            <div className='mt-3'>
              ارسال مجدد کد : <span>{count}</span>
            </div>
          )}
        </div>
      ),
    },
    {
      title: 'انتخاب کاربری',
      content: (
        <div>
          <div className={styles.step_title}>
            <span>نوع کاربری خود را انتخاب نمایید</span>
          </div>
          <Radio.Group
            value={radioValue}
            onChange={(e) => setRadioValue(e.target.value)}
          >
            <Radio className={styles.radio_style} value={1}>
              متقاضی (دریافت کننده خدمات حقوقی)
            </Radio>
            <Radio className={styles.radio_style} value={2}>
              متخصص (ارائه دهنده خدمات حقوقی) وکیل / کارشناس حقوقی
            </Radio>
          </Radio.Group>
          <div className='mt-4'>
            {radioValue === 1 && (
              <button
                className={styles.step_button}
                type='button'
                onClick={() => {
                  setVisible(false);
                  router.push('/dashboard');
                  Auth.login();
                }}
              >
                اتمام ثبت نام
              </button>
            )}
            {radioValue === 2 && (
              <button
                className={styles.step_button}
                type='button'
                onClick={() => setCurrent(current + 1)}
              >
                مرحله بعد
              </button>
            )}
          </div>
        </div>
      ),
    },
    {
      title: 'انتخاب زمینه فعالیت',
      content: (
        <div>
          <div className={styles.step_title}>
            زمینه فعالیت حقوقی خود را انتخاب نمایید.
          </div>
          <Checkbox.Group style={{ width: '100%', textAlign: 'right' }}>
            <Row>
              <Col span={8}>
                <Checkbox value='A'>دعاوی حقوقی</Checkbox>
                <Checkbox value='B'>دعاوی کیفری</Checkbox>
                <Checkbox value='C'>دعاوی دیوان عدالت </Checkbox>
                <Checkbox value='D'>دعاوی وزارت کار</Checkbox>
                <Checkbox value='E'>دعاوی بین المللی</Checkbox>
                <Checkbox value='F'>دعاوی خانواده</Checkbox>
                <Checkbox value='G'>دعاوی بیمه</Checkbox>
                <Checkbox value='H'>اختلافات مالیاتی</Checkbox>
                <Checkbox value='I'>اختلافات گمرکی</Checkbox>
              </Col>

              <Col span={8}>
                <Checkbox value='J'>امور ملکی</Checkbox>
                <Checkbox value='K'>امور ثبتی</Checkbox>
                <Checkbox value='L'>امور شرکت ها</Checkbox>
                <Checkbox value='M'>امور شهرداری</Checkbox>
                <Checkbox value='N'>امور وارث</Checkbox>
                <Checkbox value='O'>وصول مطالبات</Checkbox>
                <Checkbox value='P'>امور مهاجرت</Checkbox>
                <Checkbox value='Q'>داوری</Checkbox>
              </Col>
              <Col span={8}>
                <Checkbox value='S'>قراردادها</Checkbox>
                <Checkbox value='T'>پیمانهای دولتی</Checkbox>
                <Checkbox value='R'>سایر موارد</Checkbox>
              </Col>
            </Row>
          </Checkbox.Group>
          <div className='mt-4'>
            <button
              className={styles.step_button}
              type='button'
              onClick={() => {
                router.push('/dashboard');
                Auth.login();
              }}
            >
              ورود به پنل کاربری
            </button>
          </div>
        </div>
      ),
    },
  ];

  return (
    <>
      {Auth.isAuthenticated ? (
        <Dropdown overlay={menu} trigger={['click']}>
          <Button shape='round'>
            <span>محمد محمدی</span>
            <i className='fas fa-angle-down mr-1'></i>
          </Button>
        </Dropdown>
      ) : (
        <button className={styles.auth_button} onClick={showModal}>
          ورود یا عضویت
        </button>
      )}

      <Modal
        title={steps[current].title}
        visible={visible}
        onOk={okHandler}
        onCancel={cancelHandler}
        footer={null}
      >
        <div className={styles.auth_box}>{steps[current].content}</div>
      </Modal>
    </>
  );
}

export default Auth;

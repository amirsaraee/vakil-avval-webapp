import React from 'react';
import { Dropdown, Menu } from 'antd';
import styles from './MainNavigation.module.css';

const menuOne = (
  <Menu>
    <Menu.Item>
      <a href=''>مشاوره تلفنی با وکیل</a>
    </Menu.Item>
    <Menu.Item>
      <a href=''>ملاقات حضوری با وکیل</a>
    </Menu.Item>
    <Menu.Item>
      <a href=''>تقاضای مطالعه پرونده</a>
    </Menu.Item>
    <Menu.Item>
      <a href=''>تقاضای تنظیم اوراق قضایی</a>
    </Menu.Item>
    <Menu.Item>
      <a href=''>تقاضای تنظیم و بازبینی قرارداد</a>
    </Menu.Item>
    <Menu.Item>
      <a href=''>جستجوی وکلا و کارشناسان حقوقی</a>
    </Menu.Item>
    <Menu.Item>
      <a href=''>معرفی وکیل</a>
    </Menu.Item>
  </Menu>
);

const menuTwo = (
  <Menu>
    <Menu.Item>
      <a href=''>ورود وکلا و کارشناسان حقوقی</a>
    </Menu.Item>
    <Menu.Item>
      <a href=''>عضویت وکلا و کارشناسان حقوقی</a>
    </Menu.Item>
    <Menu.Item>
      <a href=''>امکانات وکیل اول</a>
    </Menu.Item>
  </Menu>
);

const menuThree = (
  <Menu>
    <Menu.Item>
      <a href=''>راهنمایی متقاضیان دریافت خدمات حقوقی</a>
    </Menu.Item>
    <Menu.Item>
      <a href=''>راهنمایی وکلا و کارشناسان حقوقی</a>
    </Menu.Item>
    <Menu.Item>
      <a href=''>پرسش های متداول</a>
    </Menu.Item>
    <Menu.Item>
      <a href=''>شماره تلفن پشتیبانی</a>
    </Menu.Item>
  </Menu>
);

function MainNavigation() {
  return (
    <nav className={styles.main_nav}>
      <ul>
        <li>
          <Dropdown overlay={menuOne} trigger={['click']}>
            <a
              className='ant-dropdown-link'
              onClick={(e) => e.preventDefault()}
            >
              <span>وکیل میخوام</span>
              <i className='fas fa-angle-down'></i>
            </a>
          </Dropdown>
        </li>
        <li>
          <Dropdown overlay={menuTwo}>
            <a
              className='ant-dropdown-link'
              onClick={(e) => e.preventDefault()}
            >
              <span>وکیل هستم</span>
              <i className='fas fa-angle-down'></i>
            </a>
          </Dropdown>
        </li>
        <li>
          <Dropdown overlay={menuThree}>
            <a
              className='ant-dropdown-link'
              onClick={(e) => e.preventDefault()}
            >
              <span>وکیل اول</span>
              <i className='fas fa-angle-down'></i>
            </a>
          </Dropdown>
        </li>
      </ul>
    </nav>
  );
}

export default MainNavigation;

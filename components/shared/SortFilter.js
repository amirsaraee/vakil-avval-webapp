import React, { useState, useRef } from 'react';
import useOutSideClick from '../../utils/useOutSideClick';

function SortFilter() {
  const [visible, setVisible] = useState(false);
  const ref = useRef();

  const handleShow = () => {
    setVisible(true);
  };

  useOutSideClick(ref, () => {
    if (visible) {
      setVisible(false);
    }
  });

  return (
    <div class='position-relative' ref={ref}>
      <button class='filter-btn' id='sort-btn' onClick={handleShow}>
        <i class='fas fa-sort-alt ml-1'></i>
        <span>مرتب سازی</span>
      </button>
      {visible && (
        <div class='sort-dropdown'>
          <div class='dropdown-item'>
            <i class='far fa-sort-amount-down'></i>
            <span>پیشفرض</span>
          </div>
          <div class='dropdown-item'>
            <i class='far fa-eye'></i>
            <span>پربازدیدترین ها</span>
          </div>
          <div class='dropdown-item'>
            <i class='fal fa-file-plus'></i>
            <span>جدیدترین وکلا</span>
          </div>
          <div class='dropdown-item'>
            <i class='fas fa-star'></i>
            <span>پرامتیازترین ها</span>
          </div>
          <div class='dropdown-item'>
            <i class='far fa-clock'></i>
            <span>نزدیکترین وقت مشاوره</span>
          </div>
        </div>
      )}
    </div>
  );
}

export default SortFilter;

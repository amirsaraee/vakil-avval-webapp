import React from 'react';
import Slider from 'react-slick';
import LawyerCardGridItem from '../Homepage/LawyerCardGridItem';

function PageLawyersGrid(props) {
  const settings = {
    arrows: true,
    speed: 500,
    slidesToShow: props.count,
    slidesToScroll: props.count,
    draggable: true,
    initialSlide: 0,
    infinite: false,
    rtl: true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          infinite: true,
          dots: true,
        },
      },

      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };
  return (
    <Slider {...settings}>
      <LawyerCardGridItem
        name='حسن حسنی'
        expert='وکیل پایه یک دادگستری'
        image='lawyer-1.jpg'
        comments={12}
        views={125}
      />
      <LawyerCardGridItem
        name='محمد محمدی'
        expert='کارشناس ارشد حقوق جزا'
        image='lawyer-2.jpg'
        comments={10}
        views={105}
      />
      <LawyerCardGridItem
        name='موسسه حقوقی گسترش عدالت'
        expert=''
        image='lawyer-3.jpg'
        comments={20}
        views={970}
      />
      <LawyerCardGridItem
        name='حسن حسنی'
        expert='وکیل پایه یک دادگستری'
        image='lawyer-1.jpg'
        comments={12}
        views={125}
      />
      <LawyerCardGridItem
        name='حسن حسنی'
        expert='وکیل پایه یک دادگستری'
        image='lawyer-1.jpg'
        comments={12}
        views={125}
      />
      <LawyerCardGridItem
        name='حسن حسنی'
        expert='وکیل پایه یک دادگستری'
        image='lawyer-1.jpg'
        comments={12}
        views={125}
      />
      <LawyerCardGridItem
        name='حسن حسنی'
        expert='وکیل پایه یک دادگستری'
        image='lawyer-1.jpg'
        comments={12}
        views={125}
      />
      <LawyerCardGridItem
        name='حسن حسنی'
        expert='وکیل پایه یک دادگستری'
        image='lawyer-1.jpg'
        comments={12}
        views={125}
      />
      <LawyerCardGridItem
        name='حسن حسنی'
        expert='وکیل پایه یک دادگستری'
        image='lawyer-1.jpg'
        comments={12}
        views={125}
      />
    </Slider>
  );
}

export default PageLawyersGrid;

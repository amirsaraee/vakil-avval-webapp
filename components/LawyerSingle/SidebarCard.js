import React from 'react';

function SidebarCard(props) {
  return (
    <div class='reserv-card'>
      <div class='reserv-card-head'>
        <img src='/img/set-document-icon.png' alt='' />
        <h3>{props.title}</h3>
      </div>
      <div class='reserv-card-content'>
        {props.children}
        <a href='' class='service-btn text-center w-50 mr-auto ml-auto mt-3'>
          <span>{props.button}</span>
        </a>
      </div>
    </div>
  );
}

export default SidebarCard;

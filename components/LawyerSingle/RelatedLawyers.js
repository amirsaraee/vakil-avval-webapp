import React from 'react';
import RelatedLawyerItem from './RelatedLawyerItem';

function RelatedLawyers() {
  return (
    <div class='single-lawyer-related'>
      <span class='related-title'>وکلای مرتبط با نیاز شما</span>
      <div class='related-lawyer-box'>
        <RelatedLawyerItem />
        <RelatedLawyerItem />
        <RelatedLawyerItem />
        <RelatedLawyerItem />
        <RelatedLawyerItem />
        <RelatedLawyerItem />
      </div>
    </div>
  );
}

export default RelatedLawyers;

import React from 'react';
import CommentItem from './CommentItem';

function CommenstList() {
  return (
    <div class='single-lawyer-comments-list'>
      <div>
        <div>
          <span class='single-lawyer-box-title ml-2'>نظر کاربران</span>
          <span>(71 نفر)</span>
        </div>
        <div class='comment-list-box'>
          <CommentItem />
          <CommentItem />
          <CommentItem />
          <CommentItem />
          <CommentItem />
          <CommentItem />
        </div>
        <div>
          <div class='text-center'>
            <button type='button' class='more-item-button'>
              <span>مشاهده همه 51 نظر</span>
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default CommenstList;

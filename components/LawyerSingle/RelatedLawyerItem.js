import React from 'react';

function RelatedLawyerItem() {
  return (
    <div class='related-lawyer-item'>
      <div class='related-item-content'>
        <div class='related-lawyer-image'>
          <img src='/img/lawyer-1.jpg' alt='' />
        </div>
        <div class='related-lawyer-info'>
          <div>
            <span class='item-title'>وکیل حسن حسنی</span>
          </div>
          <div>
            <span class='lawyer-expert'>وکیل پایه یک دادگستری</span>
          </div>
          <div class='related-lawyer-statistic'>
            <div class='star-rating'>
              <span class='fas fa-star star-check'></span>
              <span class='fas fa-star star-check'></span>
              <span class='fas fa-star star-check'></span>
              <span class='fas fa-star'></span>
              <span class='fas fa-star'></span>
            </div>
            <span class='mr-3'>
              21
              <i class='fal fa-user mr-1'></i>
            </span>
          </div>
        </div>
      </div>
      <div class='related-item-arrow'>
        <a href=''>
          <i class='far fa-angle-left'></i>
        </a>
      </div>
    </div>
  );
}

export default RelatedLawyerItem;

import React from 'react';

function LawyerAddress() {
  return (
    <div class='row single-lawyer-contact'>
      <div class='col-md-8'>
        <div class='lawyer-item-contact'>
          <h3>اطلاعات تماس و نشانی وکیل حسن حسنی</h3>
          <p>تهران، خیابان ولیعصر، ساختمان شماره 51</p>
          <span>
            تلفن دفتر : ****54 - 021
            <a href='' class='show-more'>
              مشاهده شماره
            </a>
          </span>
        </div>
      </div>
      <div class='col-md-4'>
        <div class='lawyer-item-map'>
          <div class='item-map-title'>
            <a href=''>
              <i class='fal fa-map-marker-alt'></i>
              <span>مشاهده نقشه و مسیر یابی</span>
            </a>
          </div>
          <div class='map-direction'>
            <a href=''>با ویز</a>
            <a href=''>با گوگل</a>
          </div>
        </div>
      </div>
    </div>
  );
}

export default LawyerAddress;

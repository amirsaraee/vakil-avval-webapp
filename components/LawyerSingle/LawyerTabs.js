import React from 'react';
import { Tabs } from 'antd';

const { TabPane } = Tabs;

function LawyerTabs() {
  return (
    <>
      <div class='single-lawyer-tab'>
        <Tabs defaultActiveKey='1'>
          <TabPane tab='تخصص ها' key='1'>
            <ul class='skills-list'>
              <li>
                <i class='fal fa-check-circle'></i>
                <span>دعاوی حقوقی</span>
              </li>
              <li>
                <i class='fal fa-check-circle'></i>
                <span>امور ورشکستگی</span>
              </li>
              <li>
                <i class='fal fa-check-circle'></i>
                <span>دارایی</span>
              </li>
              <li>
                <i class='fal fa-check-circle'></i>
                <span>دعاوی مالی</span>
              </li>
              <li>
                <i class='fal fa-check-circle'></i>
                <span>دعاوی خانوادگی</span>
              </li>
              <li>
                <i class='fal fa-check-circle'></i>
                <span>امور ثبتی</span>
              </li>
              <li>
                <i class='fal fa-check-circle'></i>
                <span>امور شرکتی</span>
              </li>
              <li>
                <i class='fal fa-check-circle'></i>
                <span>قراردادها</span>
              </li>
              <li>
                <i class='fal fa-check-circle'></i>
                <span>دعاوی ملکی</span>
              </li>
              <li>
                <i class='fal fa-check-circle'></i>
                <span>وصول مطالبات</span>
              </li>
              <li>
                <i class='fal fa-check-circle'></i>
                <span>دیوان عدالت</span>
              </li>
            </ul>
          </TabPane>
          <TabPane tab='خدمات حقوقی' key='2'>
            <ul class='skills-list'>
              <li>
                <i class='fal fa-check-circle'></i>
                <span>مشاوره تلفنی</span>
              </li>
              <li>
                <i class='fal fa-check-circle'></i>
                <span>مشاوره حضوری</span>
              </li>
              <li>
                <i class='fal fa-check-circle'></i>
                <span>مطالعه پرونده</span>
              </li>
              <li>
                <i class='fal fa-check-circle'></i>
                <span>تنظیم اوراق قضایی</span>
              </li>
              <li>
                <i class='fal fa-check-circle'></i>
                <span>تنظیم و بازبینی قرارداد</span>
              </li>
            </ul>
          </TabPane>
          <TabPane tab='سوابق حرفه ای' key='3'>
            <ul class='list-experience'>
              <li>
                <i class='fal fa-circle'></i>
                <span>کارشناس حقوقی شرکت عدالت گستر</span>
              </li>
              <li>
                <i class='fal fa-circle'></i>
                <span>ریسس امور حقوقی شرکت پردیسان</span>
              </li>
              <li>
                <i class='fal fa-circle'></i>
                <span>مدیر امور قراردادهای شرکت عصر ارتباطات</span>
              </li>
            </ul>
          </TabPane>
          <TabPane tab='سوابق تحصیلی' key='4'>
            <ul class='list-experience'>
              <li>
                <i class='fal fa-circle'></i>
                <span>کارشناسی حقوق دانشگاه تهران سال 1382</span>
              </li>
              <li>
                <i class='fal fa-circle'></i>
                <span>کارشناسی ارشد حقوق دانشگاه فردوسی سال 1387</span>
              </li>
              <li>
                <i class='fal fa-circle'></i>
                <span>دکتری حقوق دانشگاه علامه طباطبایی سال 1392</span>
              </li>
            </ul>
          </TabPane>
          <TabPane tab='گزارش عملکرد' key='5'>
            <ul class='performance-report'>
              <li>
                <div class='performance-name'>
                  <i class='fal fa-phone'></i>
                  <span>مشاوره تلفنی</span>
                </div>
                <strong class='performance-count'>12</strong>
              </li>
              <li>
                <div class='performance-name'>
                  <img src='/img/Q&A-icon.png' alt='' />
                  <span>مشاوره آنلاین</span>
                </div>
                <strong class='performance-count'>75</strong>
              </li>
              <li>
                <div class='performance-name'>
                  <i class='fas fa-male'></i>
                  <span>مشاوره حضوری</span>
                </div>
                <strong class='performance-count'>5</strong>
              </li>
              <li>
                <div class='performance-name'>
                  <img src='/img/doc-icon.png' alt='' />
                  <span>مطالعه پرونده</span>
                </div>
                <strong class='performance-count'>3</strong>
              </li>
              <li>
                <div class='performance-name'>
                  <img src='/img/set-document-icon.png' alt='' />
                  <span>تنظیم اوراق قضایی</span>
                </div>
                <strong class='performance-count'>7</strong>
              </li>
              <li>
                <div class='performance-name'>
                  <img src='/img/set-document-icon.png' alt='' />
                  <span>تنظیم قرارداد</span>
                </div>
                <strong class='performance-count'>2</strong>
              </li>
            </ul>
          </TabPane>
        </Tabs>
      </div>
    </>
  );
}

export default LawyerTabs;

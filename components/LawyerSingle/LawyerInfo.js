import React from 'react';

function LawyerInfo() {
  return (
    <div class='single-lawyer-item'>
      <div class='single-lawyer-actions'>
        <button class='action-btn'>
          <i class='fal fa-heart ml-1'></i>
          <span>ذخیره</span>
        </button>
        <button class='action-btn'>
          <i class='far fa-share-alt ml-1'></i>
          <span>اشتراک</span>
        </button>
      </div>
      <div class='laywer-item-info'>
        <div class='single-lawyer-image'>
          <img src='/img/lawyer-1.jpg' alt='' />
          <span class='single-lawyer-view'>
            50k
            <i class='far fa-eye mr-1'></i>
          </span>
        </div>
        <div class='lawyer-item-title'>
          <i class='fas fa-badge-check'></i>
          <h2>وکیل حسن حسنی</h2>
        </div>
        <div class='lawyer-item-subtitle'>
          <span>وکیل پایه یک دادگستری</span>
        </div>
        <div class='lawyer-item-skill'>
          <span>سابقه وکالت : 6 سال</span>
          <span>کارشناس ارشد حقوق</span>
          <span>
            <a href=''>
              <i class='far fa-credit-card ml-1'></i>
              پرداخت آنلاین
            </a>
          </span>
        </div>
        <div class='lawyer-item-statis'>
          <div class='star-rating'>
            <span class='fas fa-star star-check'></span>
            <span class='fas fa-star star-check'></span>
            <span class='fas fa-star star-check'></span>
            <span class='fas fa-star'></span>
            <span class='fas fa-star'></span>
          </div>
          <span>
            2100
            <i class='fal fa-user mr-1'></i>
          </span>
          <a href=''>افزودن نظر</a>
        </div>
      </div>
    </div>
  );
}

export default LawyerInfo;

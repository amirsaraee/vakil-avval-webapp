import React from 'react';

function CommentItem() {
  return (
    <div class='comment-list-item'>
      <div class='comment-list-user'>
        <div class='comment-list-user-image'>
          <i class='fas fa-user'></i>
        </div>
        <div class='comment-list-user-info'>
          <div>
            <span>نوع نوبت :</span>
            <span>حضوری</span>
          </div>
          <div>
            <span class='badge badge-success'>مشاوره شده</span>
            <span class='mr-1'>علی</span>
            <span class='mr-1'>در 21 خرداد 1398</span>
          </div>
        </div>
      </div>
      <div class='comment-list-info'>
        <div class='star-box'>
          <span class='fas fa-star star-check'></span>
          <span class='fas fa-star star-check'></span>
          <span class='fas fa-star star-check'></span>
          <span class='fas fa-star star-check'></span>
          <span class='fas fa-star'></span>
        </div>
        <span>زمان انتظار : </span>
        <span>کمتر از 30 دقیقه</span>
      </div>
      <div class='comment-list-text'>
        <p>خدمات عالی بود</p>
      </div>
    </div>
  );
}

export default CommentItem;

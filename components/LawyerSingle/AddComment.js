import React from 'react';

function AddComment() {
  return (
    <div class='single-lawyer-comment'>
      <div class='row'>
        <div class='col-md-8'>
          <h3 class='single-lawyer-box-title'>امتیاز و نظر کاربران</h3>
          <div>
            <span>قبلا تجربه مشاوره یا اعطای وکالت با وکیل را داشته اید؟</span>
          </div>
          <div>
            <span>به عملکرد ایشان چه امتیازی می دهید ؟</span>
          </div>
          <div class='commnet-star-wrap'>
            <div class='comment-star'>
              <i class='fal fa-star'></i>
              <span>1</span>
            </div>
            <div class='comment-star'>
              <i class='fal fa-star'></i>
              <span>2</span>
            </div>
            <div class='comment-star'>
              <i class='fal fa-star'></i>
              <span>3</span>
            </div>
            <div class='comment-star'>
              <i class='fal fa-star'></i>
              <span>4</span>
            </div>
            <div class='comment-star'>
              <i class='fal fa-star'></i>
              <span>5</span>
            </div>
          </div>
        </div>
        <div class='col-md-4'>
          <div>
            <img src='/img/lawyer-comment.png' alt='' />
          </div>
        </div>
      </div>
    </div>
  );
}

export default AddComment;

import React from 'react';

function CommentAnalayse() {
  return (
    <div class='single-lawyer-comment-analyse'>
      <div class='row'>
        <div class='col-md-5'>
          <div>
            <div class='review-average'>
              <span class='average-rate'>4.7</span>
              <div class='star-rating'>
                <span class='fas fa-star star-check'></span>
                <span class='fas fa-star star-check'></span>
                <span class='fas fa-star star-check'></span>
                <span class='fas fa-star star-check'></span>
                <span class='fas fa-star'></span>
              </div>
            </div>
            <div class='review-numbers'>
              <i class='far fa-user'></i>
              <span class='mr-1'>470 کاربر امتیاز داده اند</span>
            </div>
          </div>
        </div>
        <div class='col-md-7'>
          <div class='pt-4'>
            <div class='review-analyze'>
              <div>
                <span>375</span>
              </div>
              <div class='progress'>
                <div
                  class='progress-bar bg-success'
                  role='progressbar'
                  style={{ width: '80%' }}
                  aria-valuenow='100'
                  aria-valuemin='0'
                  aria-valuemax='100'
                ></div>
              </div>
              <div class='review-analayze-star'>
                <i class='fas fa-star'></i>
                <span>5</span>
              </div>
            </div>
            <div class='review-analyze'>
              <div>
                <span>120</span>
              </div>
              <div class='progress'>
                <div
                  class='progress-bar bg-primary'
                  role='progressbar'
                  style={{ width: '40%' }}
                  aria-valuenow='100'
                  aria-valuemin='0'
                  aria-valuemax='100'
                ></div>
              </div>
              <div class='review-analayze-star'>
                <i class='fas fa-star'></i>
                <span>4</span>
              </div>
            </div>
            <div class='review-analyze'>
              <div>
                <span>75</span>
              </div>
              <div class='progress'>
                <div
                  class='progress-bar bg-info'
                  role='progressbar'
                  style={{ width: '30%' }}
                  aria-valuenow='100'
                  aria-valuemin='0'
                  aria-valuemax='100'
                ></div>
              </div>
              <div class='review-analayze-star'>
                <i class='fas fa-star'></i>
                <span>3</span>
              </div>
            </div>
            <div class='review-analyze'>
              <div>
                <span>32</span>
              </div>
              <div class='progress'>
                <div
                  class='progress-bar bg-warning'
                  role='progressbar'
                  style={{ width: '20%' }}
                  aria-valuenow='100'
                  aria-valuemin='0'
                  aria-valuemax='100'
                ></div>
              </div>
              <div class='review-analayze-star'>
                <i class='fas fa-star'></i>
                <span>2</span>
              </div>
            </div>
            <div class='review-analyze'>
              <div>
                <span>10</span>
              </div>
              <div class='progress'>
                <div
                  class='progress-bar bg-danger'
                  role='progressbar'
                  style={{ width: '10%' }}
                  aria-valuenow='100'
                  aria-valuemin='0'
                  aria-valuemax='100'
                ></div>
              </div>
              <div class='review-analayze-star'>
                <i class='fas fa-star'></i>
                <span>1</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default CommentAnalayse;

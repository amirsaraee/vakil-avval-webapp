import React from 'react';

function LawyerListItem() {
  return (
    <div class='SRI-lawyer-content'>
      <div class='SRI-lawyer-image text-center'>
        <div>
          <img src='/img/lawyer-1.jpg' alt='' />
        </div>
        <div class='mt-3'>
          <span>56</span>
          <i class='far fa-eye mr-1'></i>
        </div>
      </div>
      <div class='SRI-lawyer-info'>
        <div class='SRI-lawyer-title'>
          <i class='fas fa-badge-check'></i>
          <h3>وکیل محمد محمدی</h3>
        </div>
        <div class='SRI-lawyer-exprience pt-2'>
          <span>وکیل پایه یک دادگستری</span>
        </div>
        <div class='SRI-lawyer-statis pt-2'>
          <div class='star-rating'>
            <span class='fas fa-star star-check'></span>
            <span class='fas fa-star star-check'></span>
            <span class='fas fa-star star-check'></span>
            <span class='fas fa-star'></span>
            <span class='fas fa-star'></span>
          </div>
          <span class='mr-5'>
            2100
            <i class='fal fa-user mr-1'></i>
          </span>
          <a href='' class='mr-4 SRI-lawyer-feature'>
            <i class='far fa-credit-card ml-1'></i>
            <span>پرداخت آنلاین</span>
          </a>
        </div>
        <div class='SRI-lawyer-address pt-2'>
          <span class='ml-2'>نشانی دفتر : </span>
          <p>
            وکیل آباد، بعد از بلوار دانش آموز، پلاک ۶۰۳، ساختمان پزشکان دانش
            آموز
          </p>
        </div>
        <div class='SRI-view-link'>
          <a href=''>
            <span>مشاهده پروفایل</span>
            <i class='far fa-angle-left mr-2'></i>
          </a>
        </div>
      </div>
    </div>
  );
}

export default LawyerListItem;

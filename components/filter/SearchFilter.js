import React from 'react';
import { Select, Cascader } from 'antd';
import SortFilter from '../shared/SortFilter';

const { Option } = Select;

const options = [
  {
    value: 'خراسان رضوی',
    label: 'خراسان رضوی',
    children: [
      {
        value: 'مشهد',
        label: 'مشهد',
      },
    ],
  },
  {
    value: 'تهران',
    label: 'تهران',
    children: [
      {
        value: 'تهران',
        label: 'تهران',
      },
    ],
  },
];

function SearchFilter() {
  return (
    <div class='search-filter'>
      <span>فیلترها :</span>
      <div>
        <Select
          placeholder='موضوع مشاوره'
          style={{ width: 150, textAlign: 'right' }}
        >
          <Option value='1'>امور حقیقی</Option>
          <Option value='2'>امور حقوقی</Option>
          <Option value='3'>امور ملکی</Option>
        </Select>
      </div>
      <div>
        <Select placeholder='نوع مشاوره' style={{ width: 150 }}>
          <Option value='1'>تلفنی</Option>
          <Option value='2'>حضوری</Option>
          <Option value='3'>همه</Option>
        </Select>
      </div>
      <div>
        <Cascader
          options={options}
          placeholder='استان و شهر'
          style={{ width: 170 }}
        />
      </div>
      <div>
        <Select placeholder='درجه وکیل' style={{ width: 150 }}>
          <Option value='1'>پایه یک دادگستری</Option>
          <Option value='2'>حقوق جزا</Option>
          <Option value='3'>پایه دو دادگستری</Option>
        </Select>
      </div>
      <div>
        <Select placeholder='جنسیت وکیل' style={{ width: 150 }}>
          <Option value='1'>خانم</Option>
          <Option value='2'>آقا</Option>
        </Select>
      </div>
      <SortFilter />
    </div>
  );
}

export default SearchFilter;

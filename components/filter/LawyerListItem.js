import React from 'react';
import LawyerListItemInfo from './LawyerListItemInfo';
import LawyerListReserve from './LawyerListReserve';

function LawyerListItem() {
  return (
    <div class='SRI'>
      <div class='row'>
        <div class='col-md-8'>
          <LawyerListItemInfo />
        </div>
        <div class='col-md-4'>
          <LawyerListReserve />
        </div>
      </div>
    </div>
  );
}

export default LawyerListItem;

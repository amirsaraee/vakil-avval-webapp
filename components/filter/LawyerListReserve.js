import React from 'react';

function LawyerListReserve() {
  return (
    <div class='SRI-reserv-info'>
      <div class='d-flex align-items-center'>
        <div class='SRI-reserv-icon-box'>
          <div class='SRI-reserv-icon bg-red'>
            <i class='far fa-phone'></i>
          </div>
          <div class='SRI-reserv-icon bg-green'>
            <i class='fas fa-walking'></i>
          </div>
        </div>
        <div class='mr-4'>
          <span class='d-block'>ملاقات حضوری، مشاوره تلفنی</span>
          <span>اولین وقت: 10 خرداد</span>
        </div>
      </div>
      <div class='mt-2'>
        <span>حق المشاوره: </span>
        <span>از 10/000 تا 30/000 تومان</span>
      </div>
      <div class='d-flex flex-wrap mt-3'>
        <div>
          <button
            type='button'
            class='btn service-btn'
            data-toggle='modal'
            data-target='.bd-example-modal-lg'
          >
            مشاوره تلفنی
          </button>
        </div>

        <div class='mr-3'>
          <button
            type='button'
            class='btn service-btn'
            data-toggle='modal'
            data-target='.bd-example-modal-lg'
          >
            مشاوره حضوری
          </button>
        </div>
      </div>
    </div>
  );
}

export default LawyerListReserve;

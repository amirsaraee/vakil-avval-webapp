import React from 'react';
import LawyerListItem from './LawyerListItem';

function LawyerList() {
  return (
    <div className='search-result-box'>
      <LawyerListItem />
      <LawyerListItem />
      <LawyerListItem />
    </div>
  );
}

export default LawyerList;

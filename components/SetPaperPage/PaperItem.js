import React from 'react';

function PaperItem() {
  return (
    <div class='paper-item'>
      <div class='paper-image'>
        <img src='/img/paper.jpg' alt='' />
      </div>
      <div class='paper-title'>
        <span>دادخواست الزام به تنظیم سند</span>
      </div>
      <div class='paper-footer'>
        <span>قیمت : 30000 تومان</span>
        <button
          class='btn'
          data-toggle='modal'
          data-target='.bd-example-modal-lg'
        >
          <span>مشاهده و خرید</span>
          <i class='fas fa-angle-left'></i>
        </button>
      </div>
    </div>
  );
}

export default PaperItem;

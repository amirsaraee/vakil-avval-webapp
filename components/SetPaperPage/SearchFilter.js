import React from 'react';
import { Select } from 'antd';
import SortFilter from '../shared/SortFilter';

const { Option } = Select;

function SearchFilter() {
  return (
    <div class='search-filter'>
      <span>فیلترها :</span>
      <div>
        <Select placeholder='نوع اوراق' showSearch style={{ width: 150 }}>
          <Option value='1'>درخواست</Option>
          <Option value='2'>شکواییه</Option>
          <Option value='3'>لایحه</Option>
          <Option value='4'>اظهارنامه</Option>
          <Option value='5'>همه موارد</Option>
        </Select>
      </div>
      <div>
        <Select placeholder='موضوع اوراق' showSearch style={{ width: 150 }}>
          <Option value='1'>الزام به تنظیم سند</Option>
          <Option value='2'>تحریر ترک</Option>
          <Option value='3'>همه موارد</Option>
        </Select>
      </div>
      <div>
        <Select
          placeholder='رایگان / غیر رایگان'
          showSearch
          style={{ width: 150 }}
        >
          <Option value='1'>غیر رایگان</Option>
          <Option value='2'>رایگان</Option>
          <Option value='3'>همه موارد</Option>
        </Select>
      </div>

      <SortFilter />

      <div>
        <div class='search-field'>
          <i class='fal fa-search'></i>
          <input type='text' placeholder='جستجو' />
        </div>
      </div>
    </div>
  );
}

export default SearchFilter;

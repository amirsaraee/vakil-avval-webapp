import React from 'react';

function QuestionItem() {
  return (
    <div class='latest-QA-item'>
      <div class='QA-item-content'>
        <div class='QA-title'>
          <h3>بنام زدن سند موتور سیکلت چگونه است؟</h3>
        </div>
        <div class='QA-subtitle'>
          <span>پرسیده شده : </span>
          <span class='mr-2'>2 ساعت پیش</span>
        </div>
        <div class='QA-item-description'>
          <p>
            سلام بنده موتور سیکلتی را از یکی از فامیل ها خریداری کردم و حال
            درباره سند مالکیت آن سوال دارم
          </p>
        </div>
      </div>
      <div class='QA-item-footer'>
        <div>
          <div class='d-none'>
            <i class='far fa-reply'></i>
            <a href='#!'>0 پاسخ</a>
          </div>
        </div>
        <div class='d-flex align-items-center'>
          <div>
            <i class='far fa-external-link'></i>
            <a href='#!' className='mr-1'>
              اشتراک گذاری
            </a>
          </div>
          <div class='mr-3'>
            <i class='far fa-eye ml-1'></i>
            <span>25</span>
          </div>
        </div>
      </div>
    </div>
  );
}

export default QuestionItem;

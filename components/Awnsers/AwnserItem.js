import React from 'react';
import MainButton from '../UIElements/Button';

function AwnserItem() {
  return (
    <div className='awnser-item'>
      <div className='awnser-head'>
        <div className='d-flex justify-content-between align-items-center flex-wrap'>
          <div className='awnser-lawyer'>
            <div>
              <img src='/img/lawyer-1.jpg' alt='' />
            </div>
            <div className='mr-3'>
              <span className='d-block item-title'>حسن حسنی</span>
              <span className='lawyer-expert'>وکیل پایه یک دادگستری</span>
              <div className='item-statistic'>
                <div className='star-rating'>
                  <span className='fas fa-star star-check'></span>
                  <span className='fas fa-star star-check'></span>
                  <span className='fas fa-star star-check'></span>
                  <span className='fas fa-star star-check'></span>
                  <span className='fas fa-star star-check'></span>
                </div>
                <div>
                  <span className='mr-2'>
                    <span>20</span>
                    <i className='fas fa-comments mr-1'></i>
                  </span>
                  <span className='mr-2'>
                    <span>970</span>
                    <i className='fas fa-eye mr-1'></i>
                  </span>
                </div>
              </div>
            </div>
          </div>
          <div>
            <a href='' className='btn blue-color'>
              <span>مشاهده پروفایل</span>
              <i className='fas fa-angle-left mr-1'></i>
            </a>
          </div>
        </div>
      </div>
      <div className='awnser-content'>
        <span className='awnser-time'>56 دقیقه پیش پاسخ داده است</span>
        <div className='awnser-text'>
          <p>
            لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با
            استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در
            ستون و سطرآنچنان که لازم است
          </p>
        </div>
        <div>
          <MainButton text='رزرو تماس' type='primary' auto />
          <span className='mx-1'>یا </span>
          <MainButton text='رزرو ملاقات' type='primary' auto />
          <span className='mx-1'>با وکیل حسن حسنی</span>
        </div>
      </div>
      <div className='awnser-footer'>
        <div className='d-flex justify-content-between align-items-center flex-wrap'>
          <div>
            <button type='btn' className='default-button like-btn'>
              <i className='far fa-heart'></i>
            </button>
          </div>
          <div className='awnser-actions'>
            <div>
              <i className='far fa-external-link ml-2'></i>
              <span>اشتراک گذاری</span>
            </div>
            <div className='mr-3'>
              <a href=''>
                <span>مشاده دیدگاه ها (2)</span>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default AwnserItem;

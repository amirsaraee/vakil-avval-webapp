import React from 'react';

function RelatedQuestionItem() {
  return (
    <div class='related-QA-item'>
      <div class='d-flex align-items-center'>
        <div class='related-QA-img'>
          <img src='/img/lawyer-1.jpg' alt='' />
        </div>
        <div class='mr-3'>
          <div class='awnser-text'>
            <a href=''>
              <span>بنام زدن سند موتور سکلت چگونه است؟</span>
            </a>
          </div>
          <div>
            <span class='awnser-time'>
              آخرین پاسخ توسط حسن حسنی - 10 خرداد 1399
            </span>
          </div>
        </div>
      </div>
      <div>
        <span class='related-QA-item-btn'>
          <i class='far fa-eye ml-1'></i>
          <span>1700</span>
        </span>
        <span class='related-QA-item-btn'>
          <i class='far fa-reply ml-1'></i>
          <span>20</span>
        </span>
      </div>
    </div>
  );
}

export default RelatedQuestionItem;

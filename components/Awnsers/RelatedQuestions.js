import React from 'react';
import RelatedQuestionItem from './RelatedQuestionItem';

function RelatedQuestions() {
  return (
    <div class='related-QA'>
      <span class='item-title'>پرسش و پاسخ های حقوقی مرتبط</span>
      <div class='related-QA-box'>
        <RelatedQuestionItem />
        <RelatedQuestionItem />
        <RelatedQuestionItem />
        <RelatedQuestionItem />
        <RelatedQuestionItem />
        <RelatedQuestionItem />
      </div>
    </div>
  );
}

export default RelatedQuestions;

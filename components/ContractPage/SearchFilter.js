import React from 'react';
import { Select } from 'antd';
import SortFilter from '../shared/SortFilter';

const { Option } = Select;

function SearchFilter() {
  return (
    <div class='search-filter'>
      <span>فیلترها :</span>
      <div>
        <Select
          placeholder='نوع قرارداد'
          style={{ width: 150, textAlign: 'right' }}
        >
          <Option value='1'>داخلی</Option>
          <Option value='2'>بین المللی</Option>
          <Option value='3'>همه قراردادها</Option>
        </Select>
      </div>
      <div>
        <Select
          placeholder='زبان قرارداد'
          style={{ width: 150, textAlign: 'right' }}
        >
          <Option value='1'>فارسی</Option>
          <Option value='2'>انگلیسی</Option>
          <Option value='3'>سایر زبان</Option>
          <Option value='4'>همه زبان ها</Option>
        </Select>
      </div>
      <div>
        <Select
          placeholder='رایگان / غیر رایگان'
          style={{ width: 150, textAlign: 'right' }}
        >
          <Option value='1'>غیر رایگان</Option>
          <Option value='2'>رایگان</Option>
          <Option value='3'>همه موارد</Option>
        </Select>
      </div>

      <SortFilter />
      <div>
        <div class='search-field'>
          <i class='fal fa-search'></i>
          <input type='text' placeholder='جستجو' />
        </div>
      </div>
    </div>
  );
}

export default SearchFilter;

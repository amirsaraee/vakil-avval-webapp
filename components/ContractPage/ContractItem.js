import React from 'react';

function ContractItem() {
  return (
    <div class='paper-item'>
      <div class='paper-image'>
        <img src='/img/contract-1.jpg' alt='' />
      </div>
      <div class='paper-title'>
        <span>قرارداد عاملیت فروش</span>
      </div>
      <div>
        <span>زبان قرارداد : فارسی</span>
      </div>
      <div class='paper-footer'>
        <span>قیمت : 30000 تومان</span>
        <button class='btn'>
          <span>مشاهده و خرید</span>
          <i class='fas fa-angle-left mr-1'></i>
        </button>
      </div>
    </div>
  );
}

export default ContractItem;

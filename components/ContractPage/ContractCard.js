import React, { useState } from 'react';
import { Modal, Steps, message, Button, Select } from 'antd';

const { Step } = Steps;
const { Option } = Select;

const steps = [
  {
    title: 'ارسال درخواست',
    content: (
      <fieldset>
        <div class='row align-items-center'>
          <div class='col-md-12'>
            <div class='row'>
              <div class='col-md-8'>
                <div class='d-flex align-items-center pt-2'>
                  <span>مشخصات متقاضی :</span>
                  <div class='d-flex mr-2'>
                    <div>
                      <input
                        type='text'
                        placeholder='نام و نام خانوادگی'
                        class='form-control'
                      />
                    </div>
                    <div class='mr-3'>
                      <input
                        type='text'
                        placeholder='شماره تلفن همراه'
                        class='form-control'
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div class='col-md-4'>
                <div class='d-flex pt-2'>
                  <span>جنسیت :</span>
                  <div class='mr-2'>
                    <input type='radio' name='gender' id='female' />
                    <label for='radio' className='mr-1'>
                      {' '}
                      خانم
                    </label>
                  </div>
                  <div class='mr-2'>
                    <input type='radio' name='gender' id='male' />
                    <label for='radio' className='mr-1'>
                      {' '}
                      آقا
                    </label>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class='col-md-12 pt-3 pb-3' id='first-step-modal'>
            <div class='row'>
              <div class='ml-5 pr-3'>
                <span>انتخاب کنید :</span>
              </div>
              <div class='col-md-3'>
                <Select placeholder='تنظیم / بازبینی' style={{ width: 150 }}>
                  <Option value='1'>تنظیم</Option>
                  <Option value='2'>بازبینی</Option>
                </Select>
              </div>
              <div class='col-md-3'>
                <Select placeholder='نوع قرارداد' style={{ width: 150 }}>
                  <Option value='1'>داخلی</Option>
                  <Option value='2'>بین المللی</Option>
                </Select>
              </div>
              <div class='col-md-3'>
                <Select placeholder='زبان قرارداد' style={{ width: 150 }}>
                  <Option value='1'>داخلی</Option>
                  <Option value='2'>بین المللی</Option>
                </Select>
              </div>
            </div>
          </div>

          <div class='col-md-12 mt-2 mb-4'>
            <div class='pb-2'>
              <div class='d-flex align-items-center'>
                <div class='circle-icon'>
                  <i class='fas fa-bars'></i>
                </div>
                <span class='item-title mr-2'>شرح درخواست</span>
              </div>
            </div>
            <div>
              <textarea
                name=''
                id=''
                rows='6'
                minlength='50'
                maxlength='400'
                placeholder='شرح مختصری از درخواست حقوقی خود و اطلاعات مربوطه از قبیل مشخصات مرجع قضایی، طرف دعوی و غیره را بنویسید (حداقل 50 و حداکثر 400 کاراکتر)'
                class='form-control request-text'
              ></textarea>
            </div>
          </div>
          <div class='col-md-12 mt-2 mb-4 text-right'>
            <div class='custom-file pb-5'>
              <input type='file' class='custom-file-input' id='customFile' />
              <label class='custom-file-label' for='customFile'>
                + مستندات خود را برای بررسی مشاور بارگذاری کنید (حداکثر 3 فایل و
                حداکثر حجم فایل 2 مگابایت)
              </label>
            </div>
            <span class='sub-input'>
              * فرمت های مجاز فایل جهات بارگذاری : pdf - xls - xlsx - zip - rar
              - doc - docx
            </span>
          </div>
          <div class='col-md-12 text-right'>
            <div class='request-conditon text-right'>
              <div class='QA-form-alert'>
                <i class='far fa-exclamation-circle ml-1'></i>
                <span>ملاحظات :</span>
              </div>
              <ul>
                <li>
                  <i class='far fa-circle ml-1'></i>
                  جهت تنظیم / بازبینی قرارداد درخواستی، توصیه می شود فایل
                  مستندات خود را پیوست نمایید
                </li>
                <li>
                  <i class='far fa-circle ml-1'></i>
                  مسئولیت اعتبار مستندات و صحت اطلاعات و اظهارات اعلامی بر عهده
                  متقاضی می باشد.
                </li>
                <li>
                  <i class='far fa-circle ml-1'></i>
                  مسئولیت تنظیم / بازبینی قرارداد از هر حیث صرفا بر عهده تنظیم /
                  بررسی کننده آن می باشد.
                </li>
              </ul>
            </div>
          </div>
        </div>
      </fieldset>
    ),
  },
  {
    title: 'تکمیل و تایید اطلاعات',
    content: (
      <fieldset className='text-right'>
        <span className='item-title'>ثبت نام یا ورود کاربر</span>
        <div className='row mb-5'>
          <div className='col-md-4 col-12'>
            <label for=''>انتخاب کشور</label>
            <select name='' id='' className='custom-select'>
              <option value='iran'>ایران (98+)</option>
              <option value='usa'>امریکا (1+)</option>
            </select>
          </div>
          <div className='col-md-4 col-12'>
            <label for=''>ایمیل</label>
            <input
              type='text'
              placeholder='example@example.com'
              className='form-control text-left'
            />
          </div>
          <div className='col-md-4 col-12'>
            <label for=''>تلفن همراه</label>
            <input
              type='text'
              placeholder='9123456789'
              className='form-control text-left'
            />
          </div>
        </div>
      </fieldset>
    ),
  },
  {
    title: 'ثبت درخواست و پرداخت حق المشاوره',
    content: (
      <fieldset className='text-right'>
        <div>
          <div>
            <div>
              <span>متقاضی / خریدار :</span>
              <span>آقای محمد محمدی ،</span>
              <span>تلفن : 9123446546</span>
            </div>
            <div>
              <span>موضوع سفارش : </span>
              <span>تنظیم / بازبینی قرارد</span>
            </div>
            <div>
              <span>نوع قرارداد : </span>
              <span>داخلی / بین المللی</span>
            </div>

            <div>
              <span>تنظیم کننده :</span>
              <span>وکیل حسن حسنی / وکیل اول / اداره حقوقی قوه قضاییه</span>
            </div>
            <div>
              <span>هزینه تنظیم :</span>
              <span>30 هزار تومان</span>
            </div>
          </div>

          <div className='pay-step mt-5 mb-5'>
            <div className='pay-staep-wrap'>
              <div>
                <span>پرداخت حق المشاوره</span>
              </div>
              <div className='pay-step-item money-bag'>
                <div>
                  <i className='far fa-sack-dollar ml-1'></i>
                  <span>کیف پول</span>
                </div>
                <div>
                  <span>موجودی : </span>
                  <span>0</span>
                </div>
              </div>
              <div className='pay-step-item code-discount'>
                <div className='discount-icon'>
                  <i className='fas fa-tags'></i>
                </div>
                <input type='text' placeholder='استفاده از کد تخفیف' />
                <div className='discount-button'>
                  <button type='button'>ثبت کد</button>
                </div>
              </div>
              <div className='pay-step-item select-pay'>
                <i className='far fa-credit-card'></i>
                <span className='mr-1'>انتخاب درگاه بانک عضو شتاب</span>
              </div>
              <div className='bank-btn-wrap'>
                <div className='bank-btn'>
                  <img src='/img/saman-logo.png' alt='' />
                  <input type='radio' name='bank' id='' />
                </div>
                <div className='bank-btn'>
                  <img src='/img/mellat.jpg' alt='' />
                  <input type='radio' name='bank' id='' />
                </div>
                <div className='bank-btn'>
                  <img src='/img/ap.png' alt='' />
                  <input type='radio' name='bank' />
                </div>
              </div>
              <div className='mt-5'>
                <button class='pay-btn'>پرداخت</button>
              </div>
            </div>
          </div>
        </div>
      </fieldset>
    ),
  },
];

function ContractCard(props) {
  const [visible, setVisible] = useState(false);
  const [current, setCurrent] = useState(0);

  const next = () => {
    setCurrent(current + 1);
  };

  const prev = () => {
    setCurrent(current - 1);
  };

  const showModal = () => {
    setVisible(true);
  };

  const handleOk = (e) => {
    console.log(e);
    setVisible(false);
  };

  const handleCancel = (e) => {
    console.log(e);
    setVisible(false);
  };

  return (
    <div className='card-item contract'>
      <div className='text-center'>
        <span className='text-center'>{props.type}</span>
      </div>
      <h4 className='card-title text-center'>{props.title}</h4>
      <p className='card-text'>{props.text}</p>
      <span className='card-price'>هزینه: {props.price}</span>
      <div className='text-center mt-2'>
        <button className='btn service-btn' onClick={showModal}>
          ثبت سفارش
        </button>
      </div>

      <Modal
        title={
          (current === 0 &&
            '  جهت سفارش تنظیم / بازبینی قرارداد مراحل زیر را دنبال کنید  ') ||
          (current === 1 && ' تکمیل و تایید اطلاعات ') ||
          (current === 2 && '  ملاحظه جزئیات - خرید و دانلود ورقه قضایی  ')
        }
        visible={visible}
        onOk={handleOk}
        onCancel={handleCancel}
        width={1020}
        style={{ top: 20 }}
        okButtonProps={{ hidden: true }}
        cancelButtonProps={{ hidden: true }}
      >
        <Steps current={current}>
          {steps.map((item) => (
            <Step key={item.title} title={item.title} />
          ))}
        </Steps>
        <div className='steps-content'>{steps[current].content}</div>
        <div className='steps-action'>
          {current > 0 && (
            <Button style={{ margin: '0 8px' }} onClick={() => prev()}>
              مرحله قبلی
            </Button>
          )}
          {current < steps.length - 1 && (
            <Button type='primary' onClick={() => next()}>
              تایید و ورود به مرحله بعد
            </Button>
          )}
          {current === steps.length - 1 && (
            <Button
              type='primary'
              onClick={() => {
                message.success('Processing complete!');
                setVisible(false);
              }}
            >
              اتمام
            </Button>
          )}
        </div>
      </Modal>
    </div>
  );
}

export default ContractCard;

import React from 'react';
import Link from 'next/link';

function RelatedPost() {
  return (
    <div class='related-post-item'>
      <div class='pb-2'>
        <img src='/img/blog-image.png' alt='' />
      </div>
      <Link href='/blog/[post_id]' as={`/blog/post`}>
        <a>
          <span>سند عادی چیست؟</span>
        </a>
      </Link>
    </div>
  );
}

export default RelatedPost;

import React from 'react';
import Link from 'next/link';

function PostImageItem() {
  return (
    <div class='sidebar-item-with-image'>
      <div class='d-flex align-items-center w-100'>
        <div class='sidebar-item-image'>
          <img src='/img/blog-image.png' alt='' />
        </div>
        <div class='mr-3'>
          <Link href='/blog/[post_id]' as={`/blog/post`}>
            <a>
              <span class='item-title'>سند رسمی چیست؟</span>
            </a>
          </Link>
          <div class='content-statictis pt-2 sub-input'>
            <div>
              <i class='far fa-calendar-alt ml-1'></i>
              <span>98/10/9</span>
            </div>
            <div class='mr-3'>
              <i class='far fa-eye ml-1'></i>
              <span>49</span>
            </div>
            <div class='mr-3'>
              <i class='far fa-comments ml-1'></i>
              <span>6</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default PostImageItem;

import React from 'react';
import Link from 'next/link';

function BlogPageTopItem() {
  return (
    <div class='attach-content'>
      <div class='attach-content-header'>
        <div class='attach-content-image'>
          <img src='/img/blog-image.png' alt='' />
        </div>
        <div class='attach-content-info'>
          <div class='attach-content-title pb-3'>
            <Link href='/blog/post'>
              <a class='item-title'>
                <h3>سند عادی چیست؟</h3>
              </a>
            </Link>
          </div>
          <div class='content-statictis pb-3 sub-input'>
            <div>
              <i class='far fa-calendar-alt ml-1'></i>
              <span>98/10/9</span>
            </div>
            <div class='mr-3'>
              <i class='far fa-eye ml-1'></i>
              <span>49</span>
            </div>
            <div class='mr-3'>
              <i class='far fa-comments ml-1'></i>
              <span>6</span>
            </div>
          </div>
          <div class='d-flex'>
            <div>
              <span>نویسنده :</span>
              <span>وکیل حسن حسنی</span>
            </div>
            <div class='mr-4'>
              <a href='' class='lawyer-link'>
                <span>مشاهده پروفایل</span>
                <i class='fas fa-angle-left'></i>
              </a>
            </div>
          </div>
        </div>
      </div>
      <div class='attach-content-text'>
        <span>خلاصه مطلب :</span>
        <p>
          لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده
          از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و
          سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای
          متنوع با هدف بهبود ابزارهای کاربردی می باشد.
        </p>
      </div>
      <div class='read-more-box'>
        <a href='' class='btn blue-color'>
          <span>ادامه مطلب</span>
          <i class='fas fa-angle-left'></i>
        </a>
      </div>
    </div>
  );
}

export default BlogPageTopItem;

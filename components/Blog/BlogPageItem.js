import React from 'react';
import Link from 'next/link';

function BlogPageItem() {
  return (
    <div class='BP-blog-item'>
      <div class='d-flex align-items-center'>
        <div class='BP-blog-item-image'>
          <img src='/img/blog-image-2.jpg' alt='' />
        </div>
        <div class='BP-blog-item-info'>
          <div class='pb-2'>
            <Link href='/blog/[post_id]' as={`/blog/post`}>
              <a>
                <span class='item-title'>سند رسمی چیست؟</span>
              </a>
            </Link>
          </div>
          <div class='content-statictis pb-2 sub-input'>
            <div>
              <i class='far fa-calendar-alt ml-1'></i>
              <span>98/10/9</span>
            </div>
            <div class='mr-3'>
              <i class='far fa-eye ml-1'></i>
              <span>49</span>
            </div>
            <div class='mr-3'>
              <i class='far fa-comments ml-1'></i>
              <span>6</span>
            </div>
          </div>
          <div class='BP-blog-item-text'>
            <span>خلاصه مطلب :</span>
            <p>
              لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با
              استفاده از طراحان گرافیک است.
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default BlogPageItem;

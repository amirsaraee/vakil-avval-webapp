import React from 'react';
import PostImageItem from './PostImageItem';

function SidebarImagePosts() {
  return (
    <div class='blog-page-sidebar'>
      <div class='blog-sidebar-head'>
        <h4>
          <span>پربازدیدترین مطالب ماه</span>
        </h4>
      </div>
      <div class='sidebar-items'>
        <PostImageItem />
        <PostImageItem />
        <PostImageItem />
        <PostImageItem />
        <PostImageItem />
      </div>
    </div>
  );
}

export default SidebarImagePosts;

import React from 'react';

function SidebarNewPosts() {
  return (
    <div class='blog-page-sidebar'>
      <div class='blog-sidebar-head'>
        <h4>
          <span>جدیدترین پرسش و پاسخ های حقوقی</span>
        </h4>
      </div>
      <div class='sidebar-items'>
        <div class='sidebar-item-text'>
          <a href=''>
            <i class='fas fa-angle-left'></i>
            <span>وکیل حسن حسنی در مطلب پرسش حقوقی</span>
          </a>
        </div>
        <div class='sidebar-item-text'>
          <a href=''>
            <i class='fas fa-angle-left'></i>
            <span>وکیل محمد محمدی در مطلب پرسش حقوقی</span>
          </a>
        </div>
        <div class='sidebar-item-text'>
          <a href=''>
            <i class='fas fa-angle-left'></i>
            <span>وکیل احمد احمدی در مطلب پرسش حقوقی</span>
          </a>
        </div>
        <div class='sidebar-item-text'>
          <a href=''>
            <i class='fas fa-angle-left'></i>
            <span>وکیل حسن حسنی در مطلب پرسش حقوقی</span>
          </a>
        </div>
      </div>
    </div>
  );
}

export default SidebarNewPosts;

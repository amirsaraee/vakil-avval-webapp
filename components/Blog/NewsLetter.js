import React from 'react';

function NewsLetter() {
  return (
    <div class='blog-page-sidebar'>
      <div class='blog-sidebar-head'>
        <h4>
          <span>عضویت در خبرنامه وکیل اول</span>
        </h4>
      </div>
      <div class='text-center'>
        <div class='pb-3'>
          <span>دریافت مطالب وکیل اول هر هفته در ایمیل شما</span>
        </div>

        <form action='' class='newsletter-form'>
          <input
            type='email'
            class='form-control mb-3'
            placeholder='ایمیل خود را وارد کنید'
          />
          <button type='submit' class='btn btn-block btn-success'>
            عضویت
          </button>
        </form>
      </div>
    </div>
  );
}

export default NewsLetter;

import React from 'react';
import styles from './Button.module.css';
import Link from 'next/link';

function Button({ type, block, auto, text, url, onClick }) {
  return (
    <>
      {url ? (
        <Link href={`${url}`}>
          <button
            type='button'
            className={`${styles.button} ${
              type === 'primary' && styles.primary
            } ${block && styles.block} ${auto && 'w-auto'}`}
          >
            {text}
          </button>
        </Link>
      ) : (
        <button
          type='button'
          className={`${styles.button} ${
            type === 'primary' && styles.primary
          } ${block && styles.block} ${auto && 'w-auto'}`}
          onClick={onClick}
        >
          {text}
        </button>
      )}
    </>
  );
}

export default Button;

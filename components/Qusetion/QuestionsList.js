import React from 'react';
import { Pagination } from 'antd';
import SortFilter from '../shared/SortFilter';

function QuestionsList() {
  return (
    <div className='latest-QA'>
      <div className='latest-QA-head'>
        <span>آخرین پرسش و پاسخ های حقوقی</span>
        <SortFilter />
      </div>
      <div className='latest-QA-box'>
        <div className='latest-QA-item'>
          <div className='QA-item-content'>
            <div className='QA-title'>
              <h3>بنام زدن سند موتور سیکلت چگونه است؟</h3>
            </div>
            <div className='QA-subtitle'>
              <span>پرسیده شده : </span>
              <span className='mr-2'>2 ساعت پیش</span>
            </div>
            <div className='QA-item-description'>
              <p>
                سلام بنده موتور سیکلتی را از یکی از فامیل ها خریداری کردم و حال
                درباره سند مالکیت آن سوال دارم
              </p>
            </div>
          </div>
          <div className='QA-item-footer'>
            <div>
              <i className='far fa-reply'></i>
              <a href='#!'>0 پاسخ</a>
            </div>
            <div className='d-flex align-items-center'>
              <div>
                <i className='far fa-external-link'></i>
                <a href='#!'>اشتراک گذاری</a>
              </div>
              <div className='mr-3'>
                <i className='far fa-eye'></i>
                <span>25</span>
              </div>
            </div>
          </div>
        </div>
        <div className='latest-QA-item'>
          <div className='QA-item-content'>
            <div className='QA-title'>
              <h3>بنام زدن سند موتور سیکلت چگونه است؟</h3>
            </div>
            <div className='QA-subtitle'>
              <span>پرسیده شده : </span>
              <span className='mr-2'>2 ساعت پیش</span>
            </div>
            <div className='QA-item-description'>
              <p>
                سلام بنده موتور سیکلتی را از یکی از فامیل ها خریداری کردم و حال
                درباره سند مالکیت آن سوال دارم
              </p>
            </div>
          </div>
          <div className='QA-item-footer'>
            <div>
              <i className='far fa-reply'></i>
              <a href='#!'>0 پاسخ</a>
            </div>
            <div className='d-flex align-items-center'>
              <div>
                <i className='far fa-external-link'></i>
                <a href='#!'>اشتراک گذاری</a>
              </div>
              <div className='mr-3'>
                <i className='far fa-eye'></i>
                <span>25</span>
              </div>
            </div>
          </div>
        </div>
        <div className='latest-QA-item'>
          <div className='QA-item-content'>
            <div className='QA-title'>
              <h3>بنام زدن سند موتور سیکلت چگونه است؟</h3>
            </div>
            <div className='QA-subtitle'>
              <span>پرسیده شده : </span>
              <span className='mr-2'>2 ساعت پیش</span>
            </div>
            <div className='QA-item-description'>
              <p>
                سلام بنده موتور سیکلتی را از یکی از فامیل ها خریداری کردم و حال
                درباره سند مالکیت آن سوال دارم
              </p>
            </div>
          </div>
          <div className='QA-item-footer'>
            <div>
              <i className='far fa-reply'></i>
              <a href='#!'>0 پاسخ</a>
            </div>
            <div className='d-flex align-items-center'>
              <div>
                <i className='far fa-external-link'></i>
                <a href='#!'>اشتراک گذاری</a>
              </div>
              <div className='mr-3'>
                <i className='far fa-eye'></i>
                <span>25</span>
              </div>
            </div>
          </div>
        </div>
        <div className='latest-QA-item'>
          <div className='QA-item-content'>
            <div className='QA-title'>
              <h3>بنام زدن سند موتور سیکلت چگونه است؟</h3>
            </div>
            <div className='QA-subtitle'>
              <span>پرسیده شده : </span>
              <span className='mr-2'>2 ساعت پیش</span>
            </div>
            <div className='QA-item-description'>
              <p>
                سلام بنده موتور سیکلتی را از یکی از فامیل ها خریداری کردم و حال
                درباره سند مالکیت آن سوال دارم
              </p>
            </div>
          </div>
          <div className='QA-item-footer'>
            <div>
              <i className='far fa-reply'></i>
              <a href='#!'>0 پاسخ</a>
            </div>
            <div className='d-flex align-items-center'>
              <div>
                <i className='far fa-external-link'></i>
                <a href='#!'>اشتراک گذاری</a>
              </div>
              <div className='mr-3'>
                <i className='far fa-eye'></i>
                <span>25</span>
              </div>
            </div>
          </div>
        </div>
        <div className='latest-QA-item'>
          <div className='QA-item-content'>
            <div className='QA-title'>
              <h3>بنام زدن سند موتور سیکلت چگونه است؟</h3>
            </div>
            <div className='QA-subtitle'>
              <span>پرسیده شده : </span>
              <span className='mr-2'>2 ساعت پیش</span>
            </div>
            <div className='QA-item-description'>
              <p>
                سلام بنده موتور سیکلتی را از یکی از فامیل ها خریداری کردم و حال
                درباره سند مالکیت آن سوال دارم
              </p>
            </div>
          </div>
          <div className='QA-item-footer'>
            <div>
              <i className='far fa-reply'></i>
              <a href='#!'>0 پاسخ</a>
            </div>
            <div className='d-flex align-items-center'>
              <div>
                <i className='far fa-external-link'></i>
                <a href='#!'>اشتراک گذاری</a>
              </div>
              <div className='mr-3'>
                <i className='far fa-eye'></i>
                <span>25</span>
              </div>
            </div>
          </div>
        </div>
        <div className='latest-QA-item'>
          <div className='QA-item-content'>
            <div className='QA-title'>
              <h3>بنام زدن سند موتور سیکلت چگونه است؟</h3>
            </div>
            <div className='QA-subtitle'>
              <span>پرسیده شده : </span>
              <span className='mr-2'>2 ساعت پیش</span>
            </div>
            <div className='QA-item-description'>
              <p>
                سلام بنده موتور سیکلتی را از یکی از فامیل ها خریداری کردم و حال
                درباره سند مالکیت آن سوال دارم
              </p>
            </div>
          </div>
          <div className='QA-item-footer'>
            <div>
              <i className='far fa-reply'></i>
              <a href='#!'>0 پاسخ</a>
            </div>
            <div className='d-flex align-items-center'>
              <div>
                <i className='far fa-external-link'></i>
                <a href='#!'>اشتراک گذاری</a>
              </div>
              <div className='mr-3'>
                <i className='far fa-eye'></i>
                <span>25</span>
              </div>
            </div>
          </div>
        </div>
        <div className='latest-QA-item'>
          <div className='QA-item-content'>
            <div className='QA-title'>
              <h3>بنام زدن سند موتور سیکلت چگونه است؟</h3>
            </div>
            <div className='QA-subtitle'>
              <span>پرسیده شده : </span>
              <span className='mr-2'>2 ساعت پیش</span>
            </div>
            <div className='QA-item-description'>
              <p>
                سلام بنده موتور سیکلتی را از یکی از فامیل ها خریداری کردم و حال
                درباره سند مالکیت آن سوال دارم
              </p>
            </div>
          </div>
          <div className='QA-item-footer'>
            <div>
              <i className='far fa-reply'></i>
              <a href='#!'>0 پاسخ</a>
            </div>
            <div className='d-flex align-items-center'>
              <div>
                <i className='far fa-external-link'></i>
                <a href='#!'>اشتراک گذاری</a>
              </div>
              <div className='mr-3'>
                <i className='far fa-eye'></i>
                <span>25</span>
              </div>
            </div>
          </div>
        </div>

        <div className='Search-result-pagination'>
          <Pagination defaultCurrent={1} total={50} />
        </div>
      </div>
    </div>
  );
}

export default QuestionsList;

import React from 'react';
import CardSidebar from '../shared/CardSidebar';
import MainButton from '../UIElements/Button';

function QuestionSidebar() {
  return (
    <div className='QA-sidebar'>
      <CardSidebar
        title=' مشاوره تلفنی یا حضوری '
        text=' برقراری تماس تلفنی یا رزرو ملاقات حضوری با وکیل جهت اخذ مشورت حقوقی '
        image='contact-icon.png'
      >
        <div>
          <MainButton text='رزرو تلفنی' type='primary' />
        </div>
        <div className='mr-3'>
          <MainButton text='رزرو ملاقات' type='primary' />
        </div>
      </CardSidebar>
      <CardSidebar
        title=' مطالعه پرونده '
        text=' مطالعه پرونده حقوقی یا کیفری شما، تهیه گزارش ، تنظیم و ارائه نظریه مشورتی حقوقی توسط وکیل اول '
        image='doc-icon.png'
      >
        <MainButton text='ثبت سفارش' type='primary' block />
      </CardSidebar>
      <div className='QA-sidebar-card p-0'>
        <div className='QA-sidebar-head'>
          <span className='item-title'>دسته بندی های حقوقی</span>
        </div>
        <div className='QA-sidebar-list'>
          <ul>
            <li className='sidebar-list-item'>
              <a href=''>
                <span>اسناد رسمی</span>
              </a>
              <span className='list-item-number'>120</span>
            </li>
            <li className='sidebar-list-item'>
              <a href=''>
                <span>امور ملکی</span>
              </a>
              <span className='list-item-number'>50</span>
            </li>
            <li className='sidebar-list-item'>
              <a href=''>
                <span>امور شرکت ها</span>
              </a>
              <span className='list-item-number'>25</span>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
}

export default QuestionSidebar;

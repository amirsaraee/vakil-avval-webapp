import React from 'react';

function AddQuestion() {
  return (
    <div className='QA-new-box'>
      <div className='QA-new-head'>
        <img src='/img/doc-icon.png' alt='' />
        <span>پرسش و پاسخ حقوقی آنلاین</span>
      </div>
      <form action=''>
        <div className='mt-3'>
          <label htmlFor='' className='QA-form-label'>
            عنوان پرسش
          </label>
          <input
            type='text'
            placeholder='عنوان پرسش حقوقی خود را وارد کنید'
            className='QA-form-input'
          />
        </div>
        <div className='QA-form-details'>
          <label htmlFor='' className='QA-form-label'>
            شرح پرسش{' '}
          </label>
          <textarea
            name=''
            id=''
            cols='30'
            rows='4'
            placeholder='شرح مختصری از پرسش یا مشکل حقوقی خود را بنویسید'
            className='QA-form-input'
          ></textarea>
          <div>
            <div className='QA-form-alert'>
              <i className='far fa-exclamation-circle ml-1'></i>
              <span>ملاحظات :</span>
            </div>
            <div className='QA-form-paragraphs'>
              <p>
                توصیه می شود برای دریافت پاسخ دقیق تر و سریع تر به پرسش خود از
                گرینه
                <a href='' className='terms-conditions ml-1 mr-1'>
                  مشاوره تلفنی
                </a>
                و دریافت نظر مشورتی حقوقی کاملتر و کتبی از گزینه
                <a href='' className='terms-conditions ml-1 mr-1'>
                  مطالعه پرونده
                </a>
                استفاده کنید.
              </p>
              <p>پرسش شما بطور ناشناس مطرح می شود.</p>
              <p>وارد کردن عنوان پرسش و شرح پرسش در کادرهای بالا الزامی است.</p>
            </div>
          </div>
          <div className='QA-from-action'>
            <div>
              <label for=''>
                <input type='checkbox' />
                <span>
                  <a href='' className='terms-conditions ml-1 mr-1'>
                    شرایط و قوانین
                  </a>
                  مربوط به سایت وکیل اول را می پذیرم.
                </span>
              </label>
            </div>
            <div>
              <button type='submit' className='btn QA-form-btn'>
                <span>ورود و ثبت پرسش</span>
              </button>
            </div>
          </div>
        </div>
      </form>
    </div>
  );
}

export default AddQuestion;
